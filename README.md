<h1>Swing aplikacija za laboratoriju</h1>
Projekat iz predmeta objektno orijentisano programiranje 1.
Informacioni sistem za rad laboratorije za razne analize. Korisnici ove aplikacije su zaposleni laboratorijski tehničari, medicinski tehničari, administrator sistema i pacijenti. 
Aplikacija omogućava pacijentima da:
    <ul>
        <li>zakažu određene analize kako bi olakšali i ubrzali rad zaposlenih. </li>
        <li>prate stanje završenosti uzoraka kojih su dali.</li>
        <li>pregledaju izveštaje analiza koji su obrađeni.</li>
    </ul>
    
Aplikacija omogućava zaposlenim da:
    <ul>
        <li> unose laboratorijske nalaze pacijentima.</li>
        <li> pregled zahteva za analizu.</li>
        <li> pregled prethodnih analiza pacijenata.</li>
        <li> izveštaj o dobitku, pacijentima, najčešćim analizama...</li>
    </ul>

