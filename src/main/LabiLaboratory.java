package main;


import manage.ManagerFactory;
import utils.AppSettings;
import view.LoginGUI;

public class LabiLaboratory {

	public static void main(String[] args) {
		
		System.out.println("Podesavanje aplikacije ...");
		
		String adminFile = "./data/administrators.csv";
		String labtestFile = "./data/lab_tests.csv";
		String labtechFile = "./data/lab_technicians.csv";
		String medresultFile = "./data/medical_results.csv";
		String medtechFile = "./data/medical_technicians.csv";
		String patientFile = "./data/patients.csv";
		String requestFile = "./data/requests.csv";
		String pricesFile = "./data/prices.csv";
		String testDiscountFile = "./data/test_discounts.csv";
		
		AppSettings appSettings = new AppSettings(adminFile, labtestFile, labtechFile, medresultFile, medtechFile, patientFile, requestFile, pricesFile, testDiscountFile);
		
		ManagerFactory controlers = new ManagerFactory(appSettings);
		controlers.loadData();
				
		System.out.println("Pokretanje glavnog prozora");
		LoginGUI lg = new LoginGUI(controlers);
		lg.setVisible(true);
	}

}
