package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import manage.PatientManager;

class PatientManagerTest {
	
	private static PatientManager patientManager;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		PatientManagerTest.patientManager = new PatientManager("./testData/patients.csv");
		PatientManagerTest.patientManager.loadData();
	}
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("PatientManager test end");
	}
	
	@Test
	void testsaveData() {
		assertTrue(PatientManagerTest.patientManager.saveData());
	}
	
	@Test
	void testfindById() {
		assertTrue(PatientManagerTest.patientManager.findPatientById(1) != null);
	} 

}
