package test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import entity.LabTest;
import manage.LabTestManager;
import manage.TestDiscountManager;

class TestDiscountManagerTest {
	private static TestDiscountManager testDiscountManager; 
	private static LabTestManager testMng;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		TestDiscountManagerTest.testMng = new LabTestManager("./testData/lab_tests.csv"); 
		TestDiscountManagerTest.testMng.loadData();
		TestDiscountManagerTest.testDiscountManager = new TestDiscountManager("./testData/test_discounts.csv", TestDiscountManagerTest.testMng);
		TestDiscountManagerTest.testDiscountManager.loadData();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("TestDiscountManager test end");
	}

	@Test
	void testSaveData() {
		assertTrue(TestDiscountManagerTest.testDiscountManager.saveData());
	}
	
	@Test
	void testFindById() {
		assertTrue(TestDiscountManagerTest.testDiscountManager.findById(1) != null);
	}
	
	@Test
	void testFindTestDiscount() throws ParseException {
		LabTest labTest = TestDiscountManagerTest.testMng.getLabTests().get(2);
		Date requestDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "11.06.2020.");
		assertEquals(Math.round(100*0.15), Math.round(TestDiscountManagerTest.testDiscountManager.findTestDiscount(labTest, requestDate)));
	}
	

}
