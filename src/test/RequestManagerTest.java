package test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import entity.LabTechnician;
import entity.MedicalResult;
import entity.MedicalTechnician;
import entity.Patient;
import entity.Request;
import manage.LabTechnicianManager;
import manage.LabTestManager;
import manage.MedicalResultManager;
import manage.MedicalTechnicianManager;
import manage.PatientManager;
import manage.PricesManager;
import manage.RequestManager;
import manage.TestDiscountManager;

class RequestManagerTest {
	private static RequestManager reqestManager;
	private static PatientManager patientMng;
	private static MedicalTechnicianManager medicaltechMng;
	private static LabTestManager labtestMng;
	private static LabTechnicianManager labTechMng;
	private static MedicalResultManager medicalResMng;
	private static TestDiscountManager testDiscountMng;
	private static PricesManager priceMng;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		patientMng = new PatientManager("./testData/patients.csv");
		medicaltechMng = new MedicalTechnicianManager("./testData/medical_technicians.csv");
		labtestMng = new LabTestManager("./testData/lab_tests.csv");
		labTechMng = new LabTechnicianManager("./testData/lab_technicians.csv");
		medicalResMng = new MedicalResultManager("./testData/medical_results.csv", labtestMng, labTechMng);
		testDiscountMng = new TestDiscountManager("./testData/test_discounts.csv", labtestMng);
		priceMng = new PricesManager("./testData/prices.csv");
		patientMng.loadData();
		medicaltechMng.loadData();
		labtestMng.loadData();
		labTechMng.loadData();
		medicalResMng.loadData();
		testDiscountMng.loadData();
		RequestManagerTest.reqestManager = new RequestManager("./testData/requests.csv", patientMng, medicaltechMng, medicalResMng, testDiscountMng, priceMng);
		reqestManager.loadData();
	
	}
	

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("RequestManager test end");
	}

	@Test
	void testSaveData() {
		assertTrue(reqestManager.saveData());
	}

	@Test
	void testFindRequestById() {
		assertTrue(reqestManager.findRequestById(1) != null);
	}
	
	@Test
	void testGetRequestsByPatient() {
		Patient patient = RequestManagerTest.patientMng.getPatients().get(0);
		ArrayList<Request> patientRequests = new ArrayList<Request>();
		patientRequests.add(RequestManagerTest.reqestManager.getRequests().get(0));
		
		assertEquals(patientRequests, RequestManagerTest.reqestManager.getRequestsByPatient(patient));
	}
	
	@Test
	void testNotTakenRequests() {
		assertTrue(reqestManager.getNotTakenRequests().size() == 0);
	}
	
	@Test
	void testGetMedTechMeasures() throws ParseException {
		MedicalTechnician medTech = RequestManagerTest.medicaltechMng.getMedTechnicians().get(0);
		Date fromDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "10.03.2020.");
		Date toDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "10.05.2020.");
		int dayOfWeek = 6;
		String group = "Hormoni";
		int resultNumber = 1;
		boolean homeArrival = false;
		int count = RequestManagerTest.reqestManager.getMedTechMeasures(medTech, fromDate, toDate, dayOfWeek, group, resultNumber, homeArrival);
		assertEquals(1, count);
	}
	
	@Test
	void testGetRequestsByParams() throws ParseException {
		Date fromDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "20.06.2020.");
		Date toDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "23.06.2020.");
		int dayOfWeek = 2;
		String group = "Biohemija";
		int resultNumber = 1;
		boolean homeArrival = true;
		ArrayList<Request> requests= RequestManagerTest.reqestManager.getRequestsByParams(fromDate, toDate, dayOfWeek, group, resultNumber, homeArrival);
		ArrayList<Request> testRequests = new ArrayList<Request>();
		testRequests.add(RequestManagerTest.reqestManager.getRequests().get(0));
		testRequests.add(RequestManagerTest.reqestManager.getRequests().get(5));
		assertEquals(testRequests, requests);
	}
	
	@Test
	void testGetLabTechMeasures() throws ParseException {
		LabTechnician labTech = RequestManagerTest.labTechMng.getLabTechnicians().get(0);
		Date fromDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "01.06.2020.");
		Date toDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "30.06.2020.");
		int dayOfWeek = 2;
		String group = "Biohemija";
		int resultNumber = 1;
		boolean homeArrival = true;
		int count= RequestManagerTest.reqestManager.getLabTechMeasures(labTech, fromDate, toDate, dayOfWeek, group, resultNumber, homeArrival);
		assertEquals(2, count);
	}
	
	@Test
	void testGetPatientsByDate() throws ParseException {
		Date fromDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "01.06.2020.");
		Date toDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "30.06.2020.");
		String group = "Biohemija";
		ArrayList<Patient> patients = new ArrayList<Patient>();
		patients.add(RequestManagerTest.patientMng.getPatients().get(0));
		patients.add(RequestManagerTest.patientMng.getPatients().get(1));
		patients.add(RequestManagerTest.patientMng.getPatients().get(3));
		patients.add(RequestManagerTest.patientMng.getPatients().get(2));
		assertEquals(patients, RequestManagerTest.reqestManager.getPatientsByDate(fromDate, toDate, group));
	}
	
	@Test
	void testGetOutOfRangePatientResults() throws ParseException {
		Patient patient = RequestManagerTest.patientMng.getPatients().get(1);
		ArrayList<MedicalResult> results = new ArrayList<MedicalResult>();
		results.add(RequestManagerTest.medicalResMng.getMedicalresults().get(13));
		results.add(RequestManagerTest.medicalResMng.getMedicalresults().get(7));
		assertEquals(results, RequestManagerTest.reqestManager.getOutOfRangePatientResults(patient));
	}
	
	
	@Test
	void testGetPatientTotalPrice() throws ParseException {
		Date fromDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "01.06.2020.");
		Date toDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "30.06.2020.");
		String group = "Biohemija";
		Patient patient = RequestManagerTest.patientMng.getPatients().get(1);
		assertEquals(2240, RequestManagerTest.reqestManager.getPatientTotalPrice(fromDate, toDate, group, patient));
	}
}
