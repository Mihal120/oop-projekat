package test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import entity.Employee;
import manage.ManagerFactory;
import utils.AppSettings;

class ManagerFactoryTest {
	
	private static ManagerFactory managerFactory;
	private static AppSettings appSettings;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
		String adminFile = "./testData/administrators.csv";
		String labtestFile = "./testData/lab_tests.csv";
		String labtechFile = "./testData/lab_technicians.csv";
		String medresultFile = "./testData/medical_results.csv";
		String medtechFile = "./testData/medical_technicians.csv";
		String patientFile = "./testData/patients.csv";
		String requestFile = "./testData/requests.csv";
		String pricesFile = "./testData/prices.csv";
		String testDiscountFile = "./testData/test_discounts.csv";
		ManagerFactoryTest.appSettings = new AppSettings(adminFile, labtestFile, labtechFile, medresultFile, medtechFile, patientFile, requestFile, pricesFile, testDiscountFile);
		
		ManagerFactoryTest.managerFactory = new ManagerFactory(ManagerFactoryTest.appSettings);
		ManagerFactoryTest.managerFactory.loadData();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("ManagerFactory test end");
	}

	@Test
	void testCheckLogIn() {
		assertTrue(ManagerFactoryTest.managerFactory.checkLogIn("Labi", "Krle1122") != null);
	}
	
	@Test
	void testUserExist() {
		assertTrue(ManagerFactoryTest.managerFactory.userExists("Labi"));
	}
	
	@Test
	void testGetEmployeeSalary() throws ParseException {
		Employee emp = ManagerFactoryTest.managerFactory.getLabtechMng().getLabTechnicians().get(1);
		Date fromDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "01.06.2020.");
		Date toDate = new SimpleDateFormat( "dd.MM.yyyy." ).parse( "30.06.2020.");
		int dayOfWeek = 2;
		String group = "Biohemija";
		int resultsNumber = 1;
		boolean homeArrival = false;
		float salary = ManagerFactoryTest.managerFactory.getEmployeesSalary(emp, fromDate, toDate, dayOfWeek, group, resultsNumber, homeArrival);
		assertEquals(8000, salary);
	}

}
