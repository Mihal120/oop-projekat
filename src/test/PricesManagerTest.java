package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import manage.PricesManager;

class PricesManagerTest {

	private static PricesManager priceManager;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		PricesManagerTest.priceManager = new PricesManager("./testData/prices.csv");
		PricesManagerTest.priceManager.loadData();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("PriceManager test end");
	}

	@Test
	void testSaveData() {
		assertTrue(PricesManagerTest.priceManager.saveData());
	}
	

}
