package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import manage.MedicalTechnicianManager;

class MedicalTechnicianManagerTest {
	
	private static MedicalTechnicianManager medTechManager;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		MedicalTechnicianManagerTest.medTechManager = new MedicalTechnicianManager("./testData/medical_technicians.csv");
		MedicalTechnicianManagerTest.medTechManager.loadData();
	}
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("MedicalTechnicianManager test end");
	}
	
	@Test
	void testsaveData() {
		assertTrue(MedicalTechnicianManagerTest.medTechManager.saveData());
	}
	
	@Test
	void testfindById() {
		assertTrue(MedicalTechnicianManagerTest.medTechManager.findMedTechById(1) != null);
	} 

}
