package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import manage.AdministratorManager;

class AdministratorManagerTest {
	
	private static AdministratorManager adminManager;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		AdministratorManagerTest.adminManager = new AdministratorManager("./testData/administrators.csv");
		AdministratorManagerTest.adminManager.loadData();
	}
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("AdministratorManager test end");
	}
	
	@Test
	void saveData() {
		assertTrue(AdministratorManagerTest.adminManager.saveData());
	}
	
	@Test
	void findById() {
		assertTrue(AdministratorManagerTest.adminManager.findAdminById(1) != null);
	}

}
