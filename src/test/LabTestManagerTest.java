package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import manage.LabTestManager;

class LabTestManagerTest {

	private static LabTestManager labTestManager;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		LabTestManagerTest.labTestManager = new LabTestManager("./testData/lab_tests.csv");
		LabTestManagerTest.labTestManager.loadData();
	}
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("LabTestManager test end");
	}
	
	@Test
	void saveData() {
		assertTrue(LabTestManagerTest.labTestManager.saveData());
	}
	
	@Test
	void findById() {
		assertTrue(LabTestManagerTest.labTestManager.findLabTestById(1) != null);
	} 

}
