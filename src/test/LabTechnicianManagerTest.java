package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import manage.LabTechnicianManager;

class LabTechnicianManagerTest {
	
	private static LabTechnicianManager labTechManager;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		LabTechnicianManagerTest.labTechManager = new LabTechnicianManager("./testData/lab_technicians.csv");
		LabTechnicianManagerTest.labTechManager.loadData();
		
	}
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("LabTechnicianManager test end");
	}
	
	@Test
	void saveData() {
		assertTrue(LabTechnicianManagerTest.labTechManager.saveData());
	}
	
	@Test
	void findById() {
		assertTrue(LabTechnicianManagerTest.labTechManager.findLabTechById(1) != null);
	} 
	

}
