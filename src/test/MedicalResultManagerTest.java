package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import manage.LabTechnicianManager;
import manage.LabTestManager;
import manage.MedicalResultManager;

class MedicalResultManagerTest {

	private static MedicalResultManager medResManager;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		LabTestManager labTestM = new LabTestManager("./testData/lab_tests.csv");
		labTestM.loadData();
		LabTechnicianManager labTechM = new LabTechnicianManager("./testData/lab_technicians.csv");
		labTechM.loadData();
		MedicalResultManagerTest.medResManager = new MedicalResultManager("./testData/medical_results.csv", labTestM, labTechM);
		MedicalResultManagerTest.medResManager.loadData();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("MedicalResultManager test end");
	}

	@Test
	void testSaveData() {
		assertTrue(MedicalResultManagerTest.medResManager.saveData());
	}
	
	@Test
	void testFindById() {
		assertTrue(MedicalResultManagerTest.medResManager.findMedResultById(1) != null);
	}
	
	@Test
	void testGetNextId() {
		assertEquals(36, MedicalResultManagerTest.medResManager.getNextId());
	}

}
