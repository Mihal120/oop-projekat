package entity;

import java.time.LocalDate;

public class Patient extends User{
	private String LBO;

	public Patient(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, String LBO) {
		super(id, name, surname, username, password, gender, birth_date, phone_number, address);
		this.LBO = LBO;
	}

	public String getLBO() {
		return LBO;
	}

	public void setLBO(String lBO) {
		LBO = lBO;
	}

	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((LBO == null) ? 0 : LBO.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Patient))
			return false;
		Patient other = (Patient) obj;
		if (LBO == null) {
			if (other.LBO != null)
				return false;
		} else if (!LBO.equals(other.LBO))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Patient [LBO=" + LBO + ", toString()=" + super.toString() + "]";
	}

	public String toFileString() {
		return super.toFileString() + "," + LBO;
	}

	
}
