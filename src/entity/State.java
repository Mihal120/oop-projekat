package entity;

public enum State {
	INITIAL,
	PROCESSING,
	FINISHED
}
