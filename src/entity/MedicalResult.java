package entity;

import java.time.LocalDate;

public class MedicalResult {
	private int id;
	private LabTest testType;
	private float measured_value;
	private LocalDate date;
	private LabTechnician laboratorian;
	
	MedicalResult(){}
	
	public MedicalResult(int id, LabTest testType, float measured_value, LocalDate date, LabTechnician laboratorian) {
		super();
		this.id = id;
		this.testType = testType;
		this.measured_value = measured_value;
		this.date = date;
		this.laboratorian = laboratorian;
	}

	public LabTechnician getLaboratorian() {
		return laboratorian;
	}

	public void setLaboratorian(LabTechnician laboratorian) {
		this.laboratorian = laboratorian;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LabTest getTestType() {
		return testType;
	}

	public void setTestType(LabTest test_type) {
		this.testType = test_type;
	}

	public float getMeasured_value() {
		return measured_value;
	}

	public void setMeasured_value(float measured_value) {
		this.measured_value = measured_value;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + id;
		result = prime * result + ((laboratorian == null) ? 0 : laboratorian.hashCode());
		result = prime * result + Float.floatToIntBits(measured_value);
		result = prime * result + ((testType == null) ? 0 : testType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof MedicalResult))
			return false;
		MedicalResult other = (MedicalResult) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id != other.id)
			return false;
		if (laboratorian == null) {
			if (other.laboratorian != null)
				return false;
		} else if (!laboratorian.equals(other.laboratorian))
			return false;
		if (Float.floatToIntBits(measured_value) != Float.floatToIntBits(other.measured_value))
			return false;
		if (testType == null) {
			if (other.testType != null)
				return false;
		} else if (!testType.equals(other.testType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MedicalResult [id=" + id + ", test_type=" + testType + ", measured_value=" + measured_value + ", date="
				+ date + ", laboratorian="
						+ laboratorian+"]";
	}
	
	public String toFileString() {
		String stringdate = "";
		if (date != null) {
			stringdate = date.format(Format.date_format);
		}
		String labTech = "";
		if (this.laboratorian != null) {
			labTech = String.valueOf(this.laboratorian.getId());
		}
		return  id + "," + testType.getId() + "," + String.valueOf(measured_value) + ","+ stringdate+","+labTech;
	}
	
}
