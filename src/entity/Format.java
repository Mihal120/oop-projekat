package entity;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class Format {
	public static DateTimeFormatter date_format= DateTimeFormatter.ofPattern("dd.MM.yyyy.");
	public static DateTimeFormatter date_time_format= DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm");
	public static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.");
}
