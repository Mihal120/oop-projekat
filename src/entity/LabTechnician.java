package entity;

import java.time.LocalDate;
import java.util.Arrays;

public class LabTechnician extends Employee{
	private String[] specializations;

	public LabTechnician() {}
	
	public LabTechnician(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, int qualification,  int internship, int basis_salary, int bonus_salary, String[] specializations) {
		super(id, name, surname, username, password, gender,
				birth_date, phone_number, address, qualification, internship, basis_salary, bonus_salary);
		this.specializations = specializations;
	}

	public String[] getSpecializations() {
		return specializations;
	}

	public void setSpecializations(String[] specializations) {
		this.specializations = specializations;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(specializations);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof LabTechnician))
			return false;
		LabTechnician other = (LabTechnician) obj;
		if (!Arrays.equals(specializations, other.specializations))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LabTechnician [specializations=" + specializations + ", Employee=" + super.toString() + "]";
	}
	
	public String toFileString() {
		return super.toFileString()+ "," + String.join(" ", specializations);
	}
	
}
