package entity;

import java.time.LocalDate;

public class Administrator extends User{
	public String date_pattern = "dd.MM.yyyy.";
	public Administrator(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address) {
		super(id, name, surname, username, password, gender, birth_date, phone_number, address);
	}

	public String toFileString() {
		return  getId() + "," + getName() + "," + getSurname()
				+ "," + getUsername() + "," + getPassword() + ","
				+ getGender().name() + "," + getBirth_date().format(Format.date_format) + "," + getPhone_number()
				+ "," + getAddress();
	}
	
}
