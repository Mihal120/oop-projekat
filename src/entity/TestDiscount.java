package entity;

import java.util.Date;

public class TestDiscount {
	private int id;
	private String group;
	private LabTest test;
	private float discount;
	private Date startDate;
	private Date endDate;
	private int dayOfWeek;
	
	public TestDiscount(int id, String group, LabTest test, float discount, Date startDate, Date endDate, int dayOfWeek) {
		super();
		this.id = id;
		this.group = group;
		this.test = test;
		this.discount = discount;
		this.startDate = startDate;
		this.endDate = endDate;
		this.dayOfWeek = dayOfWeek;
	}
	
	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public LabTest getTest() {
		return test;
	}
	public void setTest(LabTest test) {
		this.test = test;
	}
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(float discount) {
		this.discount = discount;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dayOfWeek;
		result = prime * result + Float.floatToIntBits(discount);
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + id;
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((test == null) ? 0 : test.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof TestDiscount))
			return false;
		TestDiscount other = (TestDiscount) obj;
		if (dayOfWeek != other.dayOfWeek)
			return false;
		if (Float.floatToIntBits(discount) != Float.floatToIntBits(other.discount))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (id != other.id)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (test == null) {
			if (other.test != null)
				return false;
		} else if (!test.equals(other.test))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TestDiscount [group=" + group + ", test=" + test + ", discount=" + discount + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", dayOfWeek="+dayOfWeek+"]";
	}
	
	public String toFileString() {
		String testId = "";
		if (test != null) {
			testId = String.valueOf(test.getId());
		}
		return id + ","+ group + "," + testId + "," + discount + "," + Format.sdf.format(startDate)+ "," + Format.sdf.format(endDate)+","+this.dayOfWeek;
	}
	
	
}
