package entity;
import java.time.LocalDate;
import java.util.HashMap;



public class Employee extends User{
	public HashMap <Integer, Integer> qualifications = new HashMap<Integer, Integer>();
	private int qualification;
	private int internship;
	private int basis_salary;
	private int bonus_salary;
	
	Employee(){}
	
	public Employee(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, 
			int qualification, int internship, int basis_salary, int bonus_salary) {
		super(id, name, surname, username, password, gender,
				birth_date, phone_number, address);
		this.qualification = qualification;
		this.internship = internship;
		this.basis_salary = basis_salary;
		this.bonus_salary = bonus_salary;
	}

	public int getQualification() {
		return qualification;
	}

	public void setQualification(int qualification) {
		this.qualification = qualification;
	}


	public int getInternship() {
		return internship;
	}

	public void setInternship(int internship) {
		this.internship = internship;
	}

	public int getBasis_salary() {
		return basis_salary;
	}

	public void setBasis_salary(int basis_salary) {
		this.basis_salary = basis_salary;
	}

	public int getBonus_salary() {
		return bonus_salary;
	}

	public void setBonus_salary(int bonus_salary) {
		this.bonus_salary = bonus_salary;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + basis_salary;
		result = prime * result + bonus_salary;
		result = prime * result + internship;
		result = prime * result + qualification;
		result = prime * result + ((qualifications == null) ? 0 : qualifications.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Employee))
			return false;
		Employee other = (Employee) obj;
		if (basis_salary != other.basis_salary)
			return false;
		if (bonus_salary != other.bonus_salary)
			return false;
		if (internship != other.internship)
			return false;
		if (qualification != other.qualification)
			return false;
		if (qualifications == null) {
			if (other.qualifications != null)
				return false;
		} else if (!qualifications.equals(other.qualifications))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString()+"Employee [qualification=" + qualification + ", internship=" + internship
				+ ", basis_salary=" + basis_salary + ", bonus_salary=" + bonus_salary + "]";
	}
	
	@Override
	public String toFileString() {
		return super.toFileString()+"," + qualification + "," + internship
				+ "," + basis_salary + "," + bonus_salary;
	}
	
}
