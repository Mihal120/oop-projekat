package entity;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import manage.PricesManager;
import manage.TestDiscountManager;

public class Request {
	private int id;
	private Patient patient;
	private boolean homeArrival;
	private LocalDateTime takenDateTime;
	private LocalDateTime finishedDateTime;
	private MedicalTechnician bloodTaker;
	private List<MedicalResult> results;
	private State state;
	
	Request(){}
	
	public Request(int id, Patient patient, boolean home_arrival, LocalDateTime takenDateTime, LocalDateTime finishedDateTime,
			MedicalTechnician bloodTaker, List<MedicalResult> results, State state) {
		this.id = id;
		this.patient = patient;
		this.homeArrival = home_arrival;
		this.takenDateTime = takenDateTime;
		this.finishedDateTime = finishedDateTime;
		this.bloodTaker = bloodTaker;
		this.results = results;
		this.state = state;
	}

		
	
	public LocalDateTime getTakenDateTime() {
		return takenDateTime;
	}

	public void setTakenDateTime(LocalDateTime taken_date_time) {
		this.takenDateTime = taken_date_time;
	}

	public LocalDateTime getFinishedDateTime() {
		return finishedDateTime;
	}

	public void setFinishedDateTime(LocalDateTime finished_date_time) {
		this.finishedDateTime = finished_date_time;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public boolean isHomeArrival() {
		return homeArrival;
	}

	public void setHomeArrival(boolean home_arrival) {
		this.homeArrival = home_arrival;
	}

	public MedicalTechnician getBloodTaker() {
		return bloodTaker;
	}

	public void setBloodTaker(MedicalTechnician blood_taker) {
		this.bloodTaker = blood_taker;
	}

	public List<MedicalResult> getResults() {
		return results;
	}

	public void setResults(List<MedicalResult> results) {
		this.results = results;
	}

	private String getResultIds() {
		ArrayList<String> ids = new ArrayList<String>();
		for (MedicalResult result : results) {
			ids.add(result.getId()+"");
		}
		return String.join(" ", ids);
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bloodTaker == null) ? 0 : bloodTaker.hashCode());
		result = prime * result + ((finishedDateTime == null) ? 0 : finishedDateTime.hashCode());
		result = prime * result + (homeArrival ? 1231 : 1237);
		result = prime * result + id;
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		result = prime * result + ((results == null) ? 0 : results.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((takenDateTime == null) ? 0 : takenDateTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Request))
			return false;
		Request other = (Request) obj;
		if (bloodTaker == null) {
			if (other.bloodTaker != null)
				return false;
		} else if (!bloodTaker.equals(other.bloodTaker))
			return false;
		if (finishedDateTime == null) {
			if (other.finishedDateTime != null)
				return false;
		} else if (!finishedDateTime.equals(other.finishedDateTime))
			return false;
		if (homeArrival != other.homeArrival)
			return false;
		if (id != other.id)
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		if (results == null) {
			if (other.results != null)
				return false;
		} else if (!results.equals(other.results))
			return false;
		if (state != other.state)
			return false;
		if (takenDateTime == null) {
			if (other.takenDateTime != null)
				return false;
		} else if (!takenDateTime.equals(other.takenDateTime))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Request [id=" + id + ", patient=" + patient + ", home_arrival=" + homeArrival + ", date_time="
				+ takenDateTime.format(Format.date_time_format) + ", finishedDateTime"+finishedDateTime.format(Format.date_time_format)+", blood_taker=" + bloodTaker + ", results=" + results + "]";
	}
	
	public String toFileString() {
		String finished = "";
		if (finishedDateTime != null) {
			finished = finishedDateTime.format(Format.date_time_format);
		}
		String taker = "";
		if (bloodTaker != null) {
			taker = String.valueOf(bloodTaker.getId());
		}
		return id + "," + patient.getId() + "," + String.valueOf(homeArrival) + ","
				+ takenDateTime.format(Format.date_time_format) + ","+ finished +"," + taker + "," + this.getResultIds() +","+ this.getState().name();
	}

	public float getSummedPrice(TestDiscountManager testDiscountMng, PricesManager priceMng) {
		float price = 0;
		ArrayList<MedicalResult> results = (ArrayList<MedicalResult>) getResults();
		for (MedicalResult result : results) {
			price += result.getTestType().getPrice() - testDiscountMng.findTestDiscount(result.getTestType(), Date.from(this.getTakenDateTime().atZone(ZoneId.systemDefault()).toInstant()));
		}
		if (this.isHomeArrival()) {
			price += priceMng.getPrices().getHomeArrivalPrice();
		}
		return price;
	}
	
	public ArrayList<String> getResultGroups() {
		ArrayList<String> groups = new ArrayList<String>();
		for (MedicalResult result : this.results) {
			groups.add(result.getTestType().getGroup());
		}
		return groups;
	}
	
	public boolean containsTest(LabTest test) {
		for (MedicalResult result : this.getResults()) {
			if (result.getTestType().getId() == test.getId())return true;
		}
		return false;
	}
	
}
