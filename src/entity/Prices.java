package entity;

import java.util.Arrays;

public class Prices {
	private float homeArrivalPrice;
	private float timePrice;
	private float[] qualificationCoefficients;
	public Prices(float homeArrivalPrice, float timePrice, float[] qualificationCoefficients) {
		super();
		this.homeArrivalPrice = homeArrivalPrice;
		this.timePrice = timePrice;
		this.qualificationCoefficients = qualificationCoefficients;
	}
	public Prices() {
		super();
	}
	public float getHomeArrivalPrice() {
		return homeArrivalPrice;
	}
	public void setHomeArrivalPrice(float homeArrivalPrice) {
		this.homeArrivalPrice = homeArrivalPrice;
	}
	public float getTimePrice() {
		return timePrice;
	}
	public void setTimePrice(float timePrice) {
		this.timePrice = timePrice;
	}
	public float[] getQualificationCoefficients() {
		return qualificationCoefficients;
	}
	public void setQualificationCoefficients(float[] qualificationCoefficient) {
		this.qualificationCoefficients = qualificationCoefficient;
	}
	@Override
	public String toString() {
		return "Prices [homeArrivalPrice=" + homeArrivalPrice + ", timePrice=" + timePrice
				+ ", qualificationCoefficient=" + Arrays.toString(qualificationCoefficients) + "]";
	}
	
	public String coefficientsToString() {
		String[] coefficientsList = new String[8];
		for (int i=0; i< coefficientsList.length;i++) {
			coefficientsList[i] = String.valueOf(this.qualificationCoefficients[i]);
		}
		return String.join(",",	coefficientsList);
	}
	
	public String toFileString() {
		return homeArrivalPrice + "," + timePrice
				+ "," + this.coefficientsToString();
	}
	
}
