package entity;

import java.time.LocalDate;

public class MedicalTechnician extends Employee{

	public MedicalTechnician(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, 
			int qualification, int internship, int basis_salary, int bonus_salary) {
		super(id, name, surname, username, password, gender,
				birth_date, phone_number, address, qualification, internship, basis_salary, bonus_salary);
	}

	@Override
	public String toString() {
		return "MedicalTechniciantoString()=" + super.toString();
	}
	
	public String toFileString() {
		return super.toFileString();
	}
}
