package entity;

public class LabTest {
	private int id;
	private String group;
	private String name;
	private int price;
	private String unit;
	private float minimum_value;
	private float maximum_value;
	
	LabTest(){}
	
	public LabTest(int id, String group, String name, int price, String unit, float minimum_value,
			float maximum_value) {
		super();
		this.id = id;
		this.group = group;
		this.name = name;
		this.price = price;
		this.unit = unit;
		this.minimum_value = minimum_value;
		this.maximum_value = maximum_value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getName() {
		return name;
	}

	public void setName(String type) {
		this.name = type;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public float getMinimum_value() {
		return minimum_value;
	}

	public void setMinimum_value(float minimum_value) {
		this.minimum_value = minimum_value;
	}

	public float getMaximum_value() {
		return maximum_value;
	}

	public void setMaximum_value(float maximum_value) {
		this.maximum_value = maximum_value;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + id;
		result = prime * result + Float.floatToIntBits(maximum_value);
		result = prime * result + Float.floatToIntBits(minimum_value);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + price;
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof LabTest))
			return false;
		LabTest other = (LabTest) obj;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (id != other.id)
			return false;
		if (Float.floatToIntBits(maximum_value) != Float.floatToIntBits(other.maximum_value))
			return false;
		if (Float.floatToIntBits(minimum_value) != Float.floatToIntBits(other.minimum_value))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price != other.price)
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BloodTest [id=" + id + ", group=" + group + ", type=" + name + ", price=" + price + ", unit=" + unit
				+ ", minimum_value=" + minimum_value + ", maximum_value=" + maximum_value + "]";
	}
	
	public String toFileString() {
		return id + "," + group + "," + name + "," + price + "," + unit+ "," + String.valueOf(minimum_value) + "," + String.valueOf(maximum_value);			
	}
	
}
