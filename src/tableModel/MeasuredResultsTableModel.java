package tableModel;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import entity.MedicalResult;
import entity.Request;

public class MeasuredResultsTableModel extends AbstractTableModel{
	
	private static final long serialVersionUID = 3937985517090995701L;
	private Request request;
	private String[] columnNames = {"Naziv", "Grupa", "Rezultat", "Referentne vrednosti", "Jedinica mere"};
	
	public MeasuredResultsTableModel(Request request) {
		this.request = request;
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.request.getResults().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		MedicalResult r = this.request.getResults().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return r.getTestType().getName();
		case 1:
			return r.getTestType().getGroup();
		case 2:
			return r.getMeasured_value();
		case 3:
			return r.getTestType().getMinimum_value()+"-"+r.getTestType().getMaximum_value();
		case 4:
			return r.getTestType().getUnit();
		default:
			return null;
		}
	}

	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	public ArrayList<MedicalResult> getRows(){
		return (ArrayList<MedicalResult>) this.request.getResults();
	}
	
	
}
