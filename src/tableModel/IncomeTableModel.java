package tableModel;

import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.AbstractTableModel;

import entity.Request;
import manage.ManagerFactory;
import manage.RequestManager;

public class IncomeTableModel extends AbstractTableModel{
	private static final long serialVersionUID = -6905888680872905030L;
	private ManagerFactory managers;
	private RequestManager requestMng;
	private ArrayList<Request> requests;
	private Date fromDate,toDate;
	private int dayOfWeek, resultNumber;
	private String group;
	private boolean homeArrival;
	private String[] columnNames = {"Id", "Broj analiza", "Ime i prezime pacijenta", "Cena"};
	
	public IncomeTableModel(ManagerFactory managers, Date fromDate, Date toDate, int dayOfWeek, String group, int resultNumber, boolean homeArrival) {
		this.requestMng = managers.getRequestMng();
		this.managers = managers;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.dayOfWeek = dayOfWeek;
		this.group = group;
		this.resultNumber = resultNumber;
		this.homeArrival = homeArrival;
		this.requests = this.requestMng.getRequestsByParams(this.fromDate, this.toDate, this.dayOfWeek, this.group, this.resultNumber, this.homeArrival);
	}
	

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.requests.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Request r = this.requests.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return r.getId();
		case 1:
			return r.getResults().size();
		case 2:
			return r.getPatient().getName()+" "+ r.getPatient().getSurname();
		case 3:
			return r.getSummedPrice(this.managers.getTestdiscountMng(), this.managers.getPricesMng());
		default:
			return null;
		}
	}
	
	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex){
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	public ArrayList<Request> getRows(){
		return this.requests;
	}
}
