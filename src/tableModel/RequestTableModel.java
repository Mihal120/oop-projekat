package tableModel;

import javax.swing.table.AbstractTableModel;

import entity.Format;
import entity.Patient;
import entity.Request;
import entity.State;
import manage.RequestManager;

public class RequestTableModel extends AbstractTableModel{
	
	private static final long serialVersionUID = -6905888680872905030L;
	private RequestManager requestMng;
	private Patient patient;
	private String[] columnNames = {"Id", "Kućna poseta", "Datum", "Status"};
	
	public RequestTableModel(RequestManager rm, Patient patient) {
		this.requestMng = rm;
		this.patient = patient;
	}
	
	public RequestTableModel(RequestManager rm) {
		this.requestMng = rm;
		this.patient = null;
	}
	

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		if (this.patient != null) {
			return this.requestMng.getRequestsByPatient(patient).size();
		}
		else {
			return this.requestMng.getRequests().size();			
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Request r;
		if (this.patient != null) {
			r = this.requestMng.getRequestsByPatient(patient).get(rowIndex);
		}
		else {
			r = this.requestMng.getRequests().get(rowIndex);
		}
		switch (columnIndex) {
		case 0:
			return r.getId();
		case 1:
			if (r.isHomeArrival())return "Da";		
			else return "Ne";
		case 2:
			return r.getTakenDateTime().format(Format.date_time_format);
		case 3:
			
			return r.getState()!=State.FINISHED?r.getState()==State.INITIAL?"Početno stanje":"U procesu":"Završeno";
		default:
			return null;
		}
	}
	
	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex){
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	
}
