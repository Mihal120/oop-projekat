package tableModel;

import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.AbstractTableModel;

import entity.Patient;
import manage.ManagerFactory;

public class PatientTableModel extends AbstractTableModel{
	
	private static final long serialVersionUID = 3937985517090995701L;
	private ArrayList<Patient> patients;
	private ManagerFactory managers;
	private Date startDate, endDate;
	private String group;
	private String[] columnNames = {"Ime", "Prezime", "Broj nalaza", "Iznos"};
	
	public PatientTableModel(ManagerFactory managers, Date startDate, Date endDate, String group) {
		this.managers = managers;
		this.startDate = startDate;
		this.endDate = endDate;
		this.group = group;
		this.patients = (ArrayList<Patient>) managers.getRequestMng().getPatientsByDate(startDate, endDate, group);
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.patients.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Patient patient = this.patients.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return patient.getName();
		case 1:
			return patient.getSurname();
		case 2:
			return this.managers.getRequestMng().getPatientRequestNumber(this.startDate, this.endDate, group, patient);
		case 3:
			return this.managers.getRequestMng().getPatientTotalPrice(this.startDate, this.endDate, this.group, patient);
		default:
			return null;
		}
	}

	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	public ArrayList<Patient> getRows(){
		return this.patients;
	}
}
