package tableModel;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import entity.LabTechnician;
import entity.MedicalResult;
import manage.RequestManager;

public class UnmeasuredResultsTableModel extends AbstractTableModel{
	private static final long serialVersionUID = 3937985517090995701L;
	private RequestManager requestMng;
	private LabTechnician labTech;
	private String[] columnNames = {"Naziv", "Grupa", "Referentne vrednosti", "Jedinica mere"};
	
	public UnmeasuredResultsTableModel(RequestManager requestMng, LabTechnician labTech) {
		this.requestMng = requestMng;
		this.labTech = labTech;
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.requestMng.getUnmeasuredForLabTech(labTech).size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		MedicalResult r = this.requestMng.getUnmeasuredForLabTech(labTech).get(rowIndex);
		switch (columnIndex) {
		case 0:
			return r.getTestType().getName();
		case 1:
			return r.getTestType().getGroup();
		case 2:
			return r.getTestType().getMinimum_value()+"-"+r.getTestType().getMaximum_value();
		case 3:
			return r.getTestType().getUnit();
		default:
			return null;
		}
	}

	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	public ArrayList<MedicalResult> getRows(){
		return this.requestMng.getUnmeasuredForLabTech(labTech);
	}
	
}
