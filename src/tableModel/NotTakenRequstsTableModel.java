package tableModel;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import entity.Request;
import entity.State;
import manage.RequestManager;

public class NotTakenRequstsTableModel extends AbstractTableModel{
	private static final long serialVersionUID = 3937985517090995701L;
	private ArrayList<Request> requests;
	private String[] columnNames = {"Id", "Kućna poseta", "Datum kreiranja", "Status"};
	
	public NotTakenRequstsTableModel(RequestManager requestMng){
		this.requests = requestMng.getNotTakenRequests();
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.requests.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Request r = this.requests.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return r.getId();
		case 1:
			if (r.isHomeArrival())return "Da";		
			else return "Ne";
		case 2:
			return r.getTakenDateTime();
		case 3:
			if (r.getState() == State.INITIAL)return "Početno stanje";
			else if(r.getState() == State.PROCESSING)return "Obrada nalaza";
			else return "Završeno";
		default:
			return null;
		}
	}

	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	public ArrayList<Request> getRows(){
		return this.requests;
	}
	
	public void remove(int index) {
		this.requests.remove(index);
		this.fireTableDataChanged();
	}
}
