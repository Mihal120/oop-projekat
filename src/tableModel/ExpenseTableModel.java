package tableModel;

import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.AbstractTableModel;

import entity.Employee;
import entity.LabTechnician;
import entity.MedicalTechnician;
import manage.ManagerFactory;

public class ExpenseTableModel extends AbstractTableModel{
	private static final long serialVersionUID = -6422821539691862350L;
	private ArrayList<Employee> employees;
	private ManagerFactory managers;
	private String[] columnNames = {"Ime", "Prezime", "Radno mesto", "Plata", "obrađeno zahteva/analiza"};
	private Date fromDate,toDate;
	private int dayOfWeek, resulNumber;
	private String group;
	private boolean homeArrival;
	
	public ExpenseTableModel(ManagerFactory managers, Date fromDate, Date toDate, int dayOfWeek, String group, int resultsNumber, boolean homeArrival) {
		this.managers = managers;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.dayOfWeek = dayOfWeek;
		this.group = group;
		this.resulNumber = resultsNumber;
		this.homeArrival = homeArrival;
		this.employees = new ArrayList<Employee>();
		this.employees.addAll(managers.getLabtechMng().getLabTechnicians());
		this.employees.addAll(managers.getMedtechMng().getMedTechnicians());
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.employees.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Employee e = this.employees.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return e.getName();
		case 1:
			return e.getSurname();
		case 2:
			if (e instanceof LabTechnician) return "Laborant";
			else return "Medicinski tehničar";
		case 3:
			return this.managers.getEmployeesSalary(e, this.fromDate, this.toDate, this.dayOfWeek, this.group, this.resulNumber, this.homeArrival);
		case 4:
			if (e instanceof LabTechnician)return this.managers.getRequestMng().getLabTechMeasures((LabTechnician) e, this.fromDate, this.toDate, this.dayOfWeek, this.group, this.resulNumber, this.homeArrival);
			else return this.managers.getRequestMng().getMedTechMeasures((MedicalTechnician) e, this.fromDate, this.toDate, this.dayOfWeek, this.group, this.resulNumber, this.homeArrival);
		default:
			return null;
		}
	}

	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	public ArrayList<Employee> getRows(){
		return this.employees;
	}
}
