package tableModel;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import entity.Employee;
import entity.LabTechnician;
import manage.ManagerFactory;

public class EmployeeTableModel extends AbstractTableModel{
	private static final long serialVersionUID = 3937985517090995701L;
	private ArrayList<Employee> employees;
	private String[] columnNames = {"Ime", "Prezime", "Radno mesto", "Stepen Stručne spreme", "Staž", "Osnova", "Bonus"};
	
	public EmployeeTableModel(ManagerFactory managers) {
		this.employees = new ArrayList<Employee>();
		this.employees.addAll(managers.getLabtechMng().getLabTechnicians());
		this.employees.addAll(managers.getMedtechMng().getMedTechnicians());
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.employees.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Employee e = this.employees.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return e.getName();
		case 1:
			return e.getSurname();
		case 2:
			if (e instanceof LabTechnician) return "Laborant";
			else return "Medicinski tehničar";
		case 3:
			return e.getQualification();
		case 4:
			return e.getInternship();
		case 5:
			return e.getBasis_salary();
		case 6:
			return e.getBonus_salary();
		default:
			return null;
		}
	}

	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	public ArrayList<Employee> getRows(){
		return this.employees;
	}

}
