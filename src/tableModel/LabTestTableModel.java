package tableModel;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import entity.LabTest;
import manage.LabTestManager;

public class LabTestTableModel extends AbstractTableModel{
	
	private static final long serialVersionUID = 6381410708557117823L;
	private	ArrayList<LabTest> tests;
	private String[] columnNames = {"Grupa", "Naziv", "Cena"};
	
	public LabTestTableModel() {
		this.tests = new ArrayList<LabTest>();
	}

	public LabTestTableModel(LabTestManager test) {
		this.tests = (ArrayList<LabTest>) test.getLabTests();
	}

	
	public LabTestTableModel(LabTestManager test, String group) {
		this.tests = test.getLabTestsByGroup(group);
	}
	
	public ArrayList<LabTest> getRows(){
		return this.tests;
	}
	
	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.tests.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		LabTest lt;
		if (this.getRowCount() != 0) {
			lt = this.tests.get(rowIndex);
		}
		else lt = null;
		switch (columnIndex) {
		case 0:
			return lt.getGroup();
		case 1:
			return lt.getName();
		case 2:
			return lt.getPrice();
		default:
			return null;
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.getValueAt(0, columnIndex).getClass();
	}

	@Override
	public String getColumnName(int column) {
		return this.columnNames[column];
	}
	
	public void addRow(LabTest test) {
		this.tests.add(test);
		this.fireTableDataChanged();
	}
	
	public void removeRow(int index) {
		this.tests.remove(index);
		this.fireTableDataChanged();
	}
	
	public boolean rowExists(LabTest test) {
		return this.tests.contains(test);
	}
	
}
