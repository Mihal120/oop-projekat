package view;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import entity.User;
import manage.ManagerFactory;
import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Window.Type;

public class LoginGUI extends JDialog {

	private static final long serialVersionUID = 1L;

	private ManagerFactory managers;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	
	public LoginGUI(ManagerFactory managers) {
		setResizable(false);
		this.managers = managers;
		initialize();
	}
		
		
		
	public void initialize() {
		setTitle("Prijava");
		setBounds(700, 350, 482, 199);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.LIGHT_GRAY);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[][][][grow]", "[][][][]"));
		{
			JLabel lblNewLabel = new JLabel("Korisni\u010Dko ime:");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
			contentPanel.add(lblNewLabel, "cell 2 1,alignx trailing");
		}
		{
			txtUsername = new JTextField();
			txtUsername.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(txtUsername, "cell 3 1,growx");
			txtUsername.setColumns(10);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("\u0160ifra:");
			lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			contentPanel.add(lblNewLabel_1, "cell 2 2,alignx trailing");
		}
		{
			txtPassword = new JPasswordField();
			txtPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(txtPassword, "cell 3 2,growx");
		}
		{
			JButton btnNewButton = new JButton("Prijava");
			btnNewButton.setForeground(new Color(0, 100, 0));
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String username = txtUsername.getText();
					String password = txtPassword.getText();
					if (username.equals("") || password.equals("")) {
						JOptionPane.showMessageDialog(null, "Morate uneti sva polja!!!", "Greška", JOptionPane.ERROR_MESSAGE);
					}
					else {
						User logged_user = managers.checkLogIn(username, password);
						if (logged_user != null) {
							setVisible(false);
							MainFrame mf = new MainFrame(managers, logged_user);
							mf.setVisible(true);													
						}
						else {
							JOptionPane.showMessageDialog(null, "Korisničko ime ili šifra ne postoje!!!", "Greška", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			});
			btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
			contentPanel.add(btnNewButton, "flowx,cell 3 3");
		}
		{
			JButton btnNewButton_1 = new JButton("Otka\u017Ei");
			btnNewButton_1.setForeground(new Color(0, 100, 0));
			btnNewButton_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					dispose();
				}
			});
			btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 14));
			contentPanel.add(btnNewButton_1, "cell 3 3,alignx right");
		}
	}
}
