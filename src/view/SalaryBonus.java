package view;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import manage.ManagerFactory;
import tableModel.EmployeeTableModel;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import entity.Employee;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SalaryBonus extends JDialog {
	private static final long serialVersionUID = 5186900399846703170L;
	private ManagerFactory managers;
	private JTable table;
	private TableRowSorter<AbstractTableModel> tableSorter = new TableRowSorter<AbstractTableModel>();

	
	public SalaryBonus(ManagerFactory managers) {
		this.managers = managers;
		initialize();
	}
	public void initialize() {
		EmployeeTableModel tableModel = new EmployeeTableModel(this.managers);
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setTitle("Promena bonusa");
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 714, 486);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 688, 353);
		getContentPane().add(scrollPane);
		
		table = new JTable(tableModel);
		table.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tableSorter.setModel(tableModel);
		table.setRowSorter(tableSorter);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Izmeni");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int viewindex = table.getSelectedRow();
				if (viewindex == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati zaposlenog!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
				}
				else {
					int index = table.convertRowIndexToModel(viewindex);
					Employee emp = tableModel.getRows().get(index);
					ChangeBonusSalary cbs = new ChangeBonusSalary(emp, managers);
					cbs.setVisible(true);
					tableModel.fireTableDataChanged();
				}
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(128, 377, 160, 54);
		getContentPane().add(btnNewButton);
		
		JButton btnIzai = new JButton("Iza\u0111i");
		btnIzai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		btnIzai.setForeground(new Color(0, 128, 0));
		btnIzai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnIzai.setBounds(382, 377, 160, 54);
		getContentPane().add(btnIzai);
	}
}
