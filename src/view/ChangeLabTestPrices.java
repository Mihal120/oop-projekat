package view;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JTable;

import entity.LabTest;
import manage.ManagerFactory;
import tableModel.LabTestTableModel;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChangeLabTestPrices extends JDialog {

	private static final long serialVersionUID = 3218307722362522093L;
	private JTable table;
	private ManagerFactory managers;

	public ChangeLabTestPrices(ManagerFactory managers) {
		this.managers = managers;
		initialize();
	}
	public void initialize() {
		LabTestTableModel tableModel = new LabTestTableModel(managers.getLabtestMng());
		setResizable(false);
		setModal(true);
		setTitle("Izmena cena analiza");
		getContentPane().setBackground(Color.LIGHT_GRAY);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 665, 362);
		getContentPane().add(scrollPane);
		
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Izmeni");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				if (index == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati analizu!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
				}
				else {
					LabTest lt = tableModel.getRows().get(index);
					InputLabTestPrice inputPrice = new InputLabTestPrice(managers, lt);
					inputPrice.setVisible(true);
					tableModel.fireTableDataChanged();
				}
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(131, 393, 149, 40);
		getContentPane().add(btnNewButton);
		
		JButton btnIzai = new JButton("Iza\u0111i");
		btnIzai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		btnIzai.setForeground(new Color(0, 128, 0));
		btnIzai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnIzai.setBounds(352, 393, 149, 40);
		getContentPane().add(btnIzai);
		setBounds(100, 100, 691, 488);
	}
}
