package view;

import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import entity.LabTechnician;
import entity.MedicalResult;
import manage.ManagerFactory;
import tableModel.UnmeasuredResultsTableModel;

import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UnmeasuredResults extends JDialog {
	private static final long serialVersionUID = -7656367473806896833L;
	private ManagerFactory managers;
	private JTable table;
	private LabTechnician labTech;
	private TableRowSorter<AbstractTableModel> tableSorter = new TableRowSorter<AbstractTableModel>();
	private UnmeasuredResultsTableModel tableModel;
	
	public UnmeasuredResults(ManagerFactory managers, LabTechnician labTech) {
		setResizable(false);
		this.managers = managers;
		this.labTech = labTech;
		initialize();
	}
	
	public void initialize() {
		tableModel = new UnmeasuredResultsTableModel(this.managers.getRequestMng(), this.labTech);
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setTitle("Neizmerene analize");
		setModal(true);
		setBounds(100, 100, 655, 489);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 37, 619, 300);
		getContentPane().add(scrollPane);
		
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Unesi rezultat");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int viewindex = table.getSelectedRow();
				if (viewindex == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati analizu!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
				}
				else {
					int index = table.convertRowIndexToModel(viewindex);
					MedicalResult result = tableModel.getRows().get(index);
					InputResult ir = new InputResult(result, managers, labTech);
					ir.setVisible(true);
					try {
						tableModel.fireTableDataChanged();						
					} catch (IndexOutOfBoundsException e) {
						JOptionPane.showMessageDialog(null, "Nema vise analiza!", "Gre�ka!", JOptionPane.INFORMATION_MESSAGE);
					}
					check();
				}
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(157, 360, 147, 52);
		getContentPane().add(btnNewButton);
		
		JButton btnIzai = new JButton("Iza\u0111i");
		btnIzai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnIzai.setForeground(new Color(0, 128, 0));
		btnIzai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnIzai.setBounds(337, 360, 147, 52);
		getContentPane().add(btnIzai);
		
		JLabel lblNewLabel = new JLabel("Izaberite za koju analizu \u017Eelite da  unesete rezultat");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 11, 368, 24);
		getContentPane().add(lblNewLabel);
		
		check();
		}

	private void check() {
		if (this.tableModel.getRowCount() == 0) {
			JOptionPane.showMessageDialog(null, "Nema rezultata koji nisu izmerena!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
			setVisible(false);
			dispose();
		}
		else {
			tableSorter.setModel((AbstractTableModel) table.getModel());
			table.setRowSorter(tableSorter);
		}
		
	}
}
