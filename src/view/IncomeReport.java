package view;


import javax.swing.JDialog;

import java.awt.Color;

import manage.ManagerFactory;
import tableModel.ExpenseTableModel;
import tableModel.IncomeTableModel;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.PieSeries.PieSeriesRenderStyle;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.style.PieStyler.AnnotationType;

import java.util.ArrayList;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;
import entity.Request;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class IncomeReport extends JDialog{

	private static final long serialVersionUID = 2417456358627815539L;
	private ManagerFactory managers;
	private JTable incomeTable;
	private JTable expenseTable;
	private JComboBox<String> dayOfWeekBox;
	private TableRowSorter<AbstractTableModel> incometableSorter = new TableRowSorter<AbstractTableModel>();
	private TableRowSorter<AbstractTableModel> expensetableSorter = new TableRowSorter<AbstractTableModel>();
	private ExpenseTableModel expenseModel;
	private IncomeTableModel incomeModel;
	private JDateChooser dateFromChooser;
	private JDateChooser dateToChooser;
	private JLabel incomelbl;
	private JLabel expenselbl;
	private JLabel profitlbl;
	private JComboBox<String> groupBox;
	private JSpinner resultSpinner;
	private JCheckBox homeArrivalCheck;
	private JPanel panel;
	private XChartPanel<PieChart> chartPane;
	
	public IncomeReport(ManagerFactory managers) {
		this.managers = managers;
		initialize();
	}
	public void initialize() {

		setResizable(false);
		setModal(true);
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setTitle("Izve\u0161taj");
		setBounds(100, 100, 1041, 638);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Od");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(31, 11, 46, 14);
		getContentPane().add(lblNewLabel);
	
		JLabel lblNewLabel_1 = new JLabel("Do");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(214, 11, 46, 14);
		getContentPane().add(lblNewLabel_1);
		
		dayOfWeekBox = new JComboBox<String>();
		dayOfWeekBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dayOfWeekBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Ponedeljak", "Utorak", "Sreda", "\u010Cetvrtak", "Petak", "Subota", "Nedelja"}));
		dayOfWeekBox.setBounds(487, 9, 132, 23);
		getContentPane().add(dayOfWeekBox);
		
		JLabel lblNewLabel_1_1 = new JLabel("Dan u nedelji");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1.setBounds(396, 11, 95, 14);
		getContentPane().add(lblNewLabel_1_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 127, 395, 185);
		getContentPane().add(scrollPane);
		
		incomeTable = new JTable();
		scrollPane.setViewportView(incomeTable);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Prihodi");
		lblNewLabel_1_1_1.setForeground(new Color(0, 128, 0));
		lblNewLabel_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_1.setBounds(10, 111, 95, 14);
		getContentPane().add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_1_2 = new JLabel("Rashodi");
		lblNewLabel_1_1_2.setForeground(new Color(255, 0, 0));
		lblNewLabel_1_1_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_2.setBounds(20, 335, 95, 14);
		getContentPane().add(lblNewLabel_1_1_2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 351, 395, 185);
		getContentPane().add(scrollPane_1);
		
		expenseTable = new JTable();
		scrollPane_1.setViewportView(expenseTable);
		
		JButton btnNewButton = new JButton("Prika\u017Ei");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				changeTables();
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(635, 31, 113, 40);
		getContentPane().add(btnNewButton);
		
		dateFromChooser = new JDateChooser();
		dateFromChooser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateFromChooser.setDateFormatString("dd.MM.yyyy.");
		dateFromChooser.setBounds(76, 11, 113, 20);
		getContentPane().add(dateFromChooser);
		
		dateToChooser = new JDateChooser();
		dateToChooser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateToChooser.setDateFormatString("dd.MM.yyyy.");
		dateToChooser.setBounds(241, 11, 113, 20);
		getContentPane().add(dateToChooser);
		
		JLabel lblNewLabel_1_1_2_1 = new JLabel("Ukupno");
		lblNewLabel_1_1_2_1.setForeground(new Color(0, 128, 0));
		lblNewLabel_1_1_2_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_2_1.setBounds(259, 315, 54, 14);
		getContentPane().add(lblNewLabel_1_1_2_1);
		
		JLabel lblNewLabel_1_1_2_2 = new JLabel("Ukupno");
		lblNewLabel_1_1_2_2.setForeground(new Color(255, 0, 0));
		lblNewLabel_1_1_2_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_2_2.setBounds(259, 538, 54, 14);
		getContentPane().add(lblNewLabel_1_1_2_2);
		
		incomelbl = new JLabel("0 din.");
		incomelbl.setForeground(new Color(0, 128, 0));
		incomelbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		incomelbl.setBounds(320, 315, 98, 14);
		getContentPane().add(incomelbl);
		
		expenselbl = new JLabel("0 din.");
		expenselbl.setForeground(new Color(255, 0, 0));
		expenselbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		expenselbl.setBounds(323, 538, 95, 14);
		getContentPane().add(expenselbl);
		
		JLabel lblNewLabel_1_1_2_2_1 = new JLabel("Profit");
		lblNewLabel_1_1_2_2_1.setForeground(new Color(30, 144, 255));
		lblNewLabel_1_1_2_2_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_2_2_1.setBounds(259, 584, 54, 14);
		getContentPane().add(lblNewLabel_1_1_2_2_1);
		
		profitlbl = new JLabel("0 din.");
		profitlbl.setForeground(new Color(30, 144, 255));
		profitlbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		profitlbl.setBounds(323, 584, 95, 14);
		getContentPane().add(profitlbl);
		
		groupBox = new JComboBox<String>();
		groupBox.setBounds(106, 46, 112, 30);
		getContentPane().add(groupBox);
		
		JLabel lblNewLabel_1_1_3 = new JLabel("Grupe analiza");
		lblNewLabel_1_1_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_3.setBounds(10, 52, 95, 14);
		getContentPane().add(lblNewLabel_1_1_3);
		
		JLabel lblNewLabel_1_1_3_1 = new JLabel("Broj zahteva ve\u0107i od");
		lblNewLabel_1_1_3_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_3_1.setBounds(251, 47, 141, 24);
		getContentPane().add(lblNewLabel_1_1_3_1);
		
		resultSpinner = new JSpinner();
		resultSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		resultSpinner.setBounds(379, 50, 39, 23);
		getContentPane().add(resultSpinner);
		
		homeArrivalCheck = new JCheckBox("Ku\u0107na poseta");
		homeArrivalCheck.setFont(new Font("Tahoma", Font.PLAIN, 14));
		homeArrivalCheck.setBackground(Color.LIGHT_GRAY);
		homeArrivalCheck.setForeground(Color.BLACK);
		homeArrivalCheck.setBounds(468, 48, 132, 23);
		getContentPane().add(homeArrivalCheck);
		
		this.panel = new JPanel();
		this.panel.setBackground(Color.GRAY);
		this.panel.setBounds(439, 141, 586, 405);
		getContentPane().add(panel);
		
		addGroupBoxValues();
	}
	
	private void viewChart() {
        
        PieChart chart = new PieChartBuilder().width(568).height(405).title("Prihodi i rashodi").build();
        
        chart.getStyler().setLegendVisible(false);
        chart.getStyler().setAnnotationType(AnnotationType.Label);
        chart.getStyler().setAnnotationDistance(.82);
        chart.getStyler().setPlotContentSize(.9);
        chart.getStyler().setDefaultSeriesRenderStyle(PieSeriesRenderStyle.Donut);
        
        String group = (String) groupBox.getSelectedItem();
        chart.addSeries(group, getIncomeSum());
        chart.addSeries("Medicinski tehničar", getMedTechSalary());
        chart.addSeries("Laborant", getLabTechSalary());
        
        chartPane = new XChartPanel<>(chart);
        this.panel.add(chartPane);
	}
	
	
	private void addGroupBoxValues() {
		ArrayList<String> groups = this.managers.getLabtestMng().getGroups();
		for (String group : groups) {
			groupBox.addItem(group);
		}
	}
	private void changeTables() {
		Date fromDate = dateFromChooser.getDate();
		Date toDate = dateToChooser.getDate();
		if (fromDate == null || toDate == null) {
			JOptionPane.showMessageDialog(null, "Izaberite datume!", "Greška!", JOptionPane.ERROR_MESSAGE);
		}
		else {
			int dayOfWeek;
			if (dayOfWeekBox.getSelectedIndex() == 6) dayOfWeek = 1;
			else dayOfWeek = dayOfWeekBox.getSelectedIndex()+2;
			String group = (String) groupBox.getSelectedItem();
			int resultsNumber = (int) resultSpinner.getValue();
			boolean homeArrival = homeArrivalCheck.isSelected();
			
			this.expenseModel = new ExpenseTableModel(managers, fromDate, toDate, dayOfWeek, group, resultsNumber, homeArrival);
			this.incomeModel = new IncomeTableModel(managers, fromDate, toDate, dayOfWeek, group, resultsNumber, homeArrival);
			if (incomeModel.getRows().size() == 0) {
				JOptionPane.showMessageDialog(null, "Nema zahteva u zadatom vremenu!", "Greška!", JOptionPane.ERROR_MESSAGE);
			}
			else {
				expensetableSorter.setModel(expenseModel);
				incometableSorter.setModel(incomeModel);
				
				incomeTable.setModel(this.incomeModel);
				expenseTable.setModel(this.expenseModel);
				incomeTable.setRowSorter(incometableSorter);
				expenseTable.setRowSorter(expensetableSorter);
				setPriceLabels();
				if (chartPane != null) {
					panel.remove(chartPane);
					panel.revalidate();
					validate();
					repaint();
				}
				viewChart();
			}
		}
	}
	
	private void setPriceLabels() {
		int incomesum = getIncomeSum();
		incomelbl.setText(String.valueOf(incomesum)+" din.");
		int expensesum = getExpenseSum();
		expenselbl.setText(String.valueOf(expensesum)+" din.");
		profitlbl.setText(String.valueOf(incomesum-expensesum)+" din.");
	}
	
	
	private int getIncomeSum() {
		int incomesum = 0;
		for (Request request : this.incomeModel.getRows()) {
			incomesum += request.getSummedPrice(managers.getTestdiscountMng(), managers.getPricesMng());
		}
		return incomesum;
	}
	private int getExpenseSum() {
		int expensesum = 0;
		for (int i = 0; i < this.expenseModel.getRows().size(); i++) {
			expensesum += (int)(float)expenseModel.getValueAt(i, 3);
		}
		return expensesum;
	}
	
	
	private int getMedTechSalary() {
		int expensesum = 0;
		for (int i = 0; i < this.expenseModel.getRows().size(); i++) {
			if (expenseModel.getValueAt(i, 2).equals("Medicinski tehničar")) {
				expensesum += (int)(float)expenseModel.getValueAt(i, 3);				
			}
		}
		return expensesum;
	}
	
	private int getLabTechSalary() {
		int expensesum = 0;
		for (int i = 0; i < this.expenseModel.getRows().size(); i++) {
			if (expenseModel.getValueAt(i, 2).equals("Laborant")) {
				expensesum += (int)(float)expenseModel.getValueAt(i, 3);				
			}
		}
		return expensesum;
	}
}
