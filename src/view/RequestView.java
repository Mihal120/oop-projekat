package view;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler.LegendPosition;
import org.knowm.xchart.style.colors.XChartSeriesColors;
import org.knowm.xchart.style.lines.SeriesLines;
import org.knowm.xchart.style.markers.SeriesMarkers;

import entity.Format;
import entity.MedicalResult;
import entity.Request;
import entity.State;
import manage.ManagerFactory;
import tableModel.MeasuredResultsTableModel;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.ListSelectionModel;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RequestView extends JDialog {

	private static final long serialVersionUID = 5679003623892054558L;
	private Request request;
	private ManagerFactory managers;
	private JDialog dialog = this;
	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private TableRowSorter<AbstractTableModel> tableSorter = new TableRowSorter<AbstractTableModel>();
	private JLabel patientNamelbl;
	private JLabel takenDateTimelbl;
	private JLabel finishedDateTimelbl;
	private JLabel medTechlbl;
	private JLabel takingWaylbl;
	private JLabel pricelbl;
	private MeasuredResultsTableModel tableModel;
	private JPanel panel;
	private XChartPanel<XYChart> chartPane;
	private XYChart chart;
	private List<Date> xData;
	private List<Double> yData;
	
	public RequestView(Request request, ManagerFactory managers) {
		setModal(true);
		this.request = request;
		this.managers = managers;
		this.initialize();
	}
		
	public void initialize() {
		tableModel = new MeasuredResultsTableModel(this.request);

		setTitle("Nalaz");
		setBounds(100, 100, 1056, 465);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.LIGHT_GRAY);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Pacijent:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(59, 50, 58, 14);
		contentPanel.add(lblNewLabel);
		
		patientNamelbl = new JLabel("New label");
		patientNamelbl.setFont(new Font("Tahoma", Font.BOLD, 13));
		patientNamelbl.setBounds(147, 50, 153, 14);
		contentPanel.add(patientNamelbl);
		
		JLabel lblNewLabel_1 = new JLabel("Uzorak uzet datuma:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(59, 86, 127, 14);
		contentPanel.add(lblNewLabel_1);
		
		takenDateTimelbl = new JLabel("New label");
		takenDateTimelbl.setFont(new Font("Tahoma", Font.BOLD, 13));
		takenDateTimelbl.setBounds(187, 86, 139, 14);
		contentPanel.add(takenDateTimelbl);
		
		JLabel lblNewLabel_2 = new JLabel("Nalaz");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_2.setBounds(178, 11, 240, 26);
		contentPanel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Nalaz zavr\u0161en datuma:");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3.setBounds(59, 120, 139, 14);
		contentPanel.add(lblNewLabel_3);
		
		finishedDateTimelbl = new JLabel("New label");
		finishedDateTimelbl.setFont(new Font("Tahoma", Font.BOLD, 13));
		finishedDateTimelbl.setBounds(206, 120, 133, 14);
		contentPanel.add(finishedDateTimelbl);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 144, 600, 187);
		contentPanel.add(scrollPane);
		
		table = new JTable(tableModel);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panel.remove(chartPane);  
		        panel.revalidate();  
		        validate();  
		        repaint();
				changeChart();
			}
		});
		table.setBorder(null);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		scrollPane.setViewportView(table);
		table.getTableHeader().setReorderingAllowed(false);
		tableSorter.setModel((AbstractTableModel) table.getModel());
		table.setRowSorter(tableSorter);
		
		JLabel lblNewLabel_4 = new JLabel("Uzeo nalaz:");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_4.setBounds(329, 50, 84, 14);
		contentPanel.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Na\u010Din dobijanja uzorka:");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_5.setBounds(329, 86, 144, 14);
		contentPanel.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Ukupna cena:");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_6.setBounds(372, 346, 90, 14);
		contentPanel.add(lblNewLabel_6);
		
		medTechlbl = new JLabel("New label");
		medTechlbl.setFont(new Font("Tahoma", Font.BOLD, 13));
		medTechlbl.setBounds(423, 50, 153, 14);
		contentPanel.add(medTechlbl);
		
		takingWaylbl = new JLabel("New label");
		takingWaylbl.setFont(new Font("Tahoma", Font.BOLD, 13));
		takingWaylbl.setBounds(483, 86, 97, 14);
		contentPanel.add(takingWaylbl);
		
		pricelbl = new JLabel("New label");
		pricelbl.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
		pricelbl.setBounds(474, 346, 106, 14);
		contentPanel.add(pricelbl);
		
		JButton btnNewButton = new JButton("Od\u0161tampaj");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveToCsv();
				JOptionPane.showMessageDialog(null, "Uspešno sačuvano u folderu data csv formata!", "Uspešno čuvanje!", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setBackground(UIManager.getColor("Button.background"));
		btnNewButton.setBounds(298, 371, 120, 42);
		contentPanel.add(btnNewButton);
		
		JButton btnIzadji = new JButton("Izadji");
		btnIzadji.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnIzadji.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		btnIzadji.setForeground(new Color(0, 128, 0));
		btnIzadji.setBackground(UIManager.getColor("Button.background"));
		btnIzadji.setBounds(549, 371, 120, 42);
		contentPanel.add(btnIzadji);
		
		this.panel = new JPanel();
		this.panel.setBackground(Color.GRAY);
		this.panel.setBounds(623, 38, 413, 303);
		contentPanel.add(this.panel);
		
		setInfo();
		
	}

	protected void changeChart() {
			int viewindex = table.getSelectedRow();
			int index = 0;
			if (viewindex != -1) {
				index = table.convertRowIndexToModel(viewindex);
			}
		
			MedicalResult selectedTest = tableModel.getRows().get(index);
			
		    
		    chart = new XYChartBuilder().width(411).height(293).title("Rezultat").xAxisTitle("Datum merenja").yAxisTitle("Izmerena vrednost").build();
		    
		    
		    chart.getStyler().setChartTitleFont(new Font(Font.MONOSPACED, Font.BOLD, 24));
		    chart.getStyler().setLegendFont(new Font(Font.SERIF, Font.PLAIN, 18));
		    chart.getStyler().setLegendPosition(LegendPosition.InsideSE);
		    chart.getStyler().setLegendSeriesLineLength(12);
		    chart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 18));
		    chart.getStyler().setAxisTickLabelsFont(new Font(Font.SERIF, Font.PLAIN, 11));
		    chart.getStyler().setDatePattern("dd.MM.yyyy.");
		    chart.getStyler().setDecimalPattern("#0.000");
		    chart.getStyler().setLocale(Locale.GERMAN);
		 
		    xData = new ArrayList<Date>();
		    yData = new ArrayList<Double>();
		 
		    this.managers.getRequestMng().getPreviuosMeasures((ArrayList<Date>)xData,(ArrayList<Double>) yData, this.request, selectedTest);
		 
		    XYSeries series = chart.addSeries("Vrednost", xData, yData);
		    series.setLineColor(XChartSeriesColors.BLUE);
		    series.setMarkerColor(Color.ORANGE);
		    series.setMarker(SeriesMarkers.CIRCLE);
		    series.setLineStyle(SeriesLines.SOLID);
		    
		    
		    chartPane = new XChartPanel<>(chart);
	        this.panel.add(chartPane);
	}

	protected void saveToCsv() {
		PrintWriter pw;
		try {
			pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream("./data/"+this.request.getPatient().getName()+"Report.csv"), "UTF-8"), false);
			pw.println("Pacijent:,"+this.request.getPatient().getName()+" "+this.request.getPatient().getSurname());
			pw.println("Uzeo uzorak:,"+this.request.getBloodTaker().getName()+" "+ this.request.getBloodTaker().getSurname());
			pw.println("Uzet dana:,"+this.request.getTakenDateTime().format(Format.date_time_format));
			pw.println("Završen dana:,"+this.request.getFinishedDateTime().format(Format.date_time_format));
			String text = this.request.isHomeArrival()?"Kućna poseta":"U ordinaciji";
			pw.println("Način uzimanja:,"+ text);
			pw.println("Cena:,"+ this.request.getSummedPrice(this.managers.getTestdiscountMng(),this.managers.getPricesMng())+" din.");
			pw.println("Naziv,Grupa,Rezultat,Referentne vrednosti,Jedinica mere");
			
			for (MedicalResult result : this.request.getResults()) {
				pw.println(result.getTestType().getName()+","+result.getTestType().getGroup()+","+result.getMeasured_value()+","+result.getTestType().getMinimum_value()+"-"+result.getTestType().getMaximum_value()+","+result.getTestType().getUnit());
			}
			pw.close();			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

	private void setInfo() {
		if (this.request.getState() == State.INITIAL) {
			JOptionPane.showMessageDialog(null, "Zahtev još nije uzet!", "Greška!", JOptionPane.ERROR_MESSAGE);
			dialog.setVisible(false);
			dialog.dispose();
		}
		else {
			patientNamelbl.setText(this.request.getPatient().getName()+" "+this.request.getPatient().getSurname());
			takenDateTimelbl.setText(this.request.getTakenDateTime().format(Format.date_time_format));
			finishedDateTimelbl.setText(this.request.getFinishedDateTime().format(Format.date_time_format));
			medTechlbl.setText(this.request.getBloodTaker().getName()+" "+ this.request.getBloodTaker().getSurname());
			String text = this.request.isHomeArrival()?"Kućna poseta":"U ordinaciji";
			takingWaylbl.setText(text);
			pricelbl.setText(this.request.getSummedPrice(this.managers.getTestdiscountMng(), this.managers.getPricesMng())+" din.");
			changeChart();
		}
	}
}
