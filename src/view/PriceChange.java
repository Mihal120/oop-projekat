package view;


import javax.swing.JDialog;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import manage.PricesManager;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class PriceChange extends JDialog {
	private static final long serialVersionUID = -5106503409486735538L;
	private PricesManager priceMng;
	private JSpinner timePriceSpinner;
	private JSpinner homePriceSpinner;
	private JSpinner qualificationSpinner;
	private JSpinner coefficientSpinner;

	public PriceChange(PricesManager priceMng) {
		this.priceMng = priceMng;
		initialize();
	}
	public void initialize() {
		setTitle("Izmena cena");
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 470, 423);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cena ku\u0107ne posete:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(101, 38, 136, 29);
		getContentPane().add(lblNewLabel);
		
		JLabel lblCenaTanogVremena = new JLabel("Cena ta\u010Dnog vremena:");
		lblCenaTanogVremena.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCenaTanogVremena.setBounds(83, 88, 154, 29);
		getContentPane().add(lblCenaTanogVremena);
		
		homePriceSpinner = new JSpinner();
		homePriceSpinner.setFont(new Font("Tahoma", Font.PLAIN, 13));
		homePriceSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		homePriceSpinner.setBounds(247, 40, 93, 29);
		getContentPane().add(homePriceSpinner);
		
		timePriceSpinner = new JSpinner();
		timePriceSpinner.setFont(new Font("Tahoma", Font.PLAIN, 13));
		timePriceSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		timePriceSpinner.setBounds(247, 88, 93, 29);
		getContentPane().add(timePriceSpinner);
		
		JLabel lblStepenStruneSpreme = new JLabel("Stepen stru\u010Dne spreme");
		lblStepenStruneSpreme.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblStepenStruneSpreme.setBounds(101, 156, 167, 29);
		getContentPane().add(lblStepenStruneSpreme);
		
		JLabel lblNjgeovKoeficijent = new JLabel("Njegov koeficijent");
		lblNjgeovKoeficijent.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNjgeovKoeficijent.setBounds(278, 156, 167, 29);
		getContentPane().add(lblNjgeovKoeficijent);
		
		qualificationSpinner = new JSpinner();
		qualificationSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				int index = (int)qualificationSpinner.getValue();
				coefficientSpinner.setValue(priceMng.getPrices().getQualificationCoefficients()[index-1]);
			}
		});
		qualificationSpinner.setModel(new SpinnerNumberModel(1, 1, 8, 1));
		qualificationSpinner.setBounds(161, 196, 60, 35);
		getContentPane().add(qualificationSpinner);
		
		coefficientSpinner = new JSpinner();
		coefficientSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				priceMng.editQualificationCoefficient((int)qualificationSpinner.getValue()-1, (float)coefficientSpinner.getValue());
			}
		});
		coefficientSpinner.setModel(new SpinnerNumberModel(new Float(1), new Float(1), null, new Float(0.1)));
		coefficientSpinner.setBounds(298, 196, 83, 35);
		getContentPane().add(coefficientSpinner);
		
		JButton btnNewButton = new JButton("Sa\u010Duvaj");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				float homeArrivalPrice = (float) homePriceSpinner.getValue();
				float timePrice = (float)timePriceSpinner.getValue();
				priceMng.edit(homeArrivalPrice, timePrice, priceMng.getPrices().getQualificationCoefficients());
				JOptionPane.showMessageDialog(null, "Uspe�no izmenjeni podaci!", "Uspe�no", JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				dispose();
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(83, 286, 118, 55);
		getContentPane().add(btnNewButton);
		
		JButton btnOtkai = new JButton("Otka\u017Ei");
		btnOtkai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		btnOtkai.setForeground(new Color(0, 128, 0));
		btnOtkai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnOtkai.setBounds(263, 286, 118, 55);
		getContentPane().add(btnOtkai);
		
		setVaues();
	}

	private void setVaues() {
		homePriceSpinner.setValue(this.priceMng.getPrices().getHomeArrivalPrice());
		timePriceSpinner.setValue(this.priceMng.getPrices().getTimePrice());
		int index = (int)qualificationSpinner.getValue();
		coefficientSpinner.setValue(priceMng.getPrices().getQualificationCoefficients()[index-1]);
	}
}
