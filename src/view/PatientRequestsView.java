package view;


import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import entity.Patient;
import entity.Request;
import manage.ManagerFactory;
import tableModel.RequestTableModel;

import java.util.ArrayList;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.UIManager;

public class PatientRequestsView extends JDialog {
	private static final long serialVersionUID = 1L;
	private ManagerFactory managers;
	private JTable table;
	private Patient patient;
	private TableRowSorter<AbstractTableModel> tableSorter = new TableRowSorter<AbstractTableModel>();

	public PatientRequestsView(ManagerFactory managers, Patient patient) {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 13));
		setModal(true);
		this.managers = managers;
		this.patient = patient;
		this.initialize();
	}
	
	public void initialize() {
		RequestTableModel tableModel = new RequestTableModel(this.managers.getRequestMng(), this.patient);
		setTitle("Zahtevi pacijenta");
		setBounds(100, 100, 659, 448);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 623, 335);
		getContentPane().add(scrollPane);
		
		table = new JTable(tableModel);
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getTableHeader().setReorderingAllowed(false);
		tableSorter.setModel((AbstractTableModel) table.getModel());
		table.setRowSorter(tableSorter);
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Prikazi nalaz");
		btnNewButton.setForeground(new Color(0, 100, 0));
		btnNewButton.setBackground(UIManager.getColor("Button.background"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int viewindex = table.getSelectedRow();
				if (viewindex == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati zahtev!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
				}
				else {
					int index = table.convertRowIndexToModel(viewindex);
					ArrayList<Request> requests;
					if (patient == null) {
						requests = (ArrayList<Request>) managers.getRequestMng().getRequests();
					}
					else {
						requests = managers.getRequestMng().getRequestsByPatient(patient);
					}
					RequestView rv = new RequestView(requests.get(index), managers);
					rv.setVisible(true);					
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(147, 357, 138, 41);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Izadji");
		btnNewButton_1.setForeground(new Color(0, 128, 0));
		btnNewButton_1.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_1.setBounds(323, 357, 127, 41);
		getContentPane().add(btnNewButton_1);

	}
}
