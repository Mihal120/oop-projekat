package view;


import javax.swing.JDialog;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import entity.MedicalTechnician;
import entity.Request;
import entity.State;
import manage.ManagerFactory;
import tableModel.NotTakenRequstsTableModel;

public class NotTakenRequests extends JDialog {
	private static final long serialVersionUID = 2724694166116081024L;
	private ManagerFactory managers;
	private MedicalTechnician technician;
	private JTable table;
	private TableRowSorter<AbstractTableModel> tableSorter = new TableRowSorter<AbstractTableModel>();
	
	
	public NotTakenRequests(ManagerFactory managers, MedicalTechnician technicianMng) {
		this.managers = managers;
		this.technician = technicianMng;
		initialize();
	}
	public void initialize() {	
		NotTakenRequstsTableModel tableModel = new NotTakenRequstsTableModel(this.managers.getRequestMng());
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setTitle("Pregled ne uzetih analiza");
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 650, 472);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 39, 624, 312);
		getContentPane().add(scrollPane);
		
		table = new JTable(tableModel);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("Izaberite zahtev koji ste uzeli");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 11, 232, 25);
		getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Ozna\u010Di kao uzet");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTaken(tableModel);
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(138, 362, 155, 50);
		getContentPane().add(btnNewButton);
		
		JButton btnIzai = new JButton("Iza\u0111i");
		btnIzai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnIzai.setForeground(new Color(0, 128, 0));
		btnIzai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnIzai.setBounds(360, 360, 155, 50);
		getContentPane().add(btnIzai);
		
		check(tableModel);
	}
	private void check(NotTakenRequstsTableModel tableModel) {
		if (tableModel.getRowCount() == 0) {
			JOptionPane.showMessageDialog(null, "Trenutno nema nalaza koji nisu uzeti!", "Nema nalaza!", JOptionPane.INFORMATION_MESSAGE);
			setVisible(false);
			dispose();
		}
		else {
			tableSorter.setModel((AbstractTableModel) table.getModel());
			table.setRowSorter(tableSorter);
		}
	}
	private void setTaken(NotTakenRequstsTableModel tableModel) {
		int viewindex = table.getSelectedRow();
		if (viewindex == -1) {
			JOptionPane.showMessageDialog(null, "Morate izabrati zahtev!", "Greška!", JOptionPane.ERROR_MESSAGE);
		}
		else {
			int index = table.convertRowIndexToModel(viewindex);
			LocalDateTime takenDateTime = LocalDateTime.now();
			State state = State.PROCESSING;
			Request request = tableModel.getRows().get(index);
			request.setBloodTaker(technician);
			request.setTakenDateTime(takenDateTime);
			request.setState(state);
			managers.getRequestMng().edit(request.getId(), request.getPatient(), request.isHomeArrival(), request.getTakenDateTime(), request.getFinishedDateTime(), request.getBloodTaker(), request.getResults(), state);
			JOptionPane.showMessageDialog(null, "Uspešno označen zahtev!", "Uspešno označeno!", JOptionPane.INFORMATION_MESSAGE);
			if (tableModel.getRowCount() == 1) {
				JOptionPane.showMessageDialog(null, "Nema više zahteva!", "Nema zahteva!", JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				dispose();
			}
			else {
				tableModel.remove(index);						
			}
		}
	}
}
