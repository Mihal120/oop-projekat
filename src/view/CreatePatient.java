package view;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.awt.event.ActionEvent;

import entity.Format;
import entity.Gender;
import manage.ManagerFactory;

import com.toedter.calendar.JDateChooser;

public class CreatePatient extends JDialog {
	
	private static final long serialVersionUID = -8425731277027044256L;
	private ManagerFactory managers;
	private JTextField nametxt;
	private JTextField surnametxt;
	private JTextField usernametxt;
	private JPasswordField passwordtxt;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField phoneNumbertxt;
	private JTextField addresstxt;
	private JTextField lbotxt;
	private JRadioButton maleRadio;
	private JRadioButton femaleRadio;
	private JDateChooser dateChooser;
	
	public CreatePatient(ManagerFactory managers){
		this.managers = managers;
		initialize();
	}
		
	public void initialize() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setTitle("Kreiranje pacijenta");
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 509, 491);
		getContentPane().setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("Ime:");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel.setBounds(165, 38, 58, 14);
			getContentPane().add(lblNewLabel);
		}
		{
			JLabel lblPrezime = new JLabel("Prezime:");
			lblPrezime.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblPrezime.setBounds(146, 69, 96, 14);
			getContentPane().add(lblPrezime);
		}
		{
			JLabel lblKorisnikoIme = new JLabel("Korisni\u010Dko ime:");
			lblKorisnikoIme.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblKorisnikoIme.setBounds(105, 101, 118, 14);
			getContentPane().add(lblKorisnikoIme);
		}
		{
			JLabel lblifra = new JLabel("\u0160ifra:");
			lblifra.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblifra.setBounds(160, 138, 44, 14);
			getContentPane().add(lblifra);
		}
		{
			JLabel lblPol = new JLabel("Pol:");
			lblPol.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblPol.setBounds(160, 171, 44, 14);
			getContentPane().add(lblPol);
		}
		{
			JLabel lblDatumRoenja = new JLabel("Datum ro\u0111enja:");
			lblDatumRoenja.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblDatumRoenja.setBounds(95, 201, 118, 14);
			getContentPane().add(lblDatumRoenja);
		}
		{
			JLabel lblBrTelefona = new JLabel("Br. telefona:");
			lblBrTelefona.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblBrTelefona.setBounds(115, 234, 108, 14);
			getContentPane().add(lblBrTelefona);
		}
		{
			JLabel lblAdresa = new JLabel("Adresa:");
			lblAdresa.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblAdresa.setBounds(146, 270, 58, 14);
			getContentPane().add(lblAdresa);
		}
		{
			JLabel lblLbo = new JLabel("LBO:");
			lblLbo.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblLbo.setBounds(154, 304, 50, 14);
			getContentPane().add(lblLbo);
		}
		
		nametxt = new JTextField();
		nametxt.setBounds(204, 37, 138, 20);
		getContentPane().add(nametxt);
		nametxt.setColumns(10);
		
		surnametxt = new JTextField();
		surnametxt.setColumns(10);
		surnametxt.setBounds(204, 68, 138, 20);
		getContentPane().add(surnametxt);
		
		usernametxt = new JTextField();
		usernametxt.setColumns(10);
		usernametxt.setBounds(204, 100, 138, 20);
		getContentPane().add(usernametxt);
		
		passwordtxt = new JPasswordField();
		passwordtxt.setBounds(204, 137, 138, 20);
		getContentPane().add(passwordtxt);
		
		maleRadio = new JRadioButton("Mu\u0161ko");
		maleRadio.setSelected(true);
		maleRadio.setBackground(Color.LIGHT_GRAY);
		buttonGroup.add(maleRadio);
		maleRadio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		maleRadio.setBounds(208, 167, 85, 23);
		getContentPane().add(maleRadio);
		
		femaleRadio = new JRadioButton("\u017Densko");
		femaleRadio.setBackground(Color.LIGHT_GRAY);
		buttonGroup.add(femaleRadio);
		femaleRadio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		femaleRadio.setBounds(289, 167, 109, 23);
		getContentPane().add(femaleRadio);
		
		phoneNumbertxt = new JTextField();
		phoneNumbertxt.setColumns(10);
		phoneNumbertxt.setBounds(204, 233, 138, 20);
		getContentPane().add(phoneNumbertxt);
		
		addresstxt = new JTextField();
		addresstxt.setColumns(10);
		addresstxt.setBounds(204, 269, 138, 20);
		getContentPane().add(addresstxt);
		
		lbotxt = new JTextField();
		lbotxt.setColumns(10);
		lbotxt.setBounds(204, 303, 138, 20);
		getContentPane().add(lbotxt);
		
		JButton btnNewButton = new JButton("Kreiraj");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkData();
				
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(87, 357, 155, 50);
		getContentPane().add(btnNewButton);
		
		JButton btnOtkai = new JButton("Otka\u017Ei");
		btnOtkai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnOtkai.setForeground(new Color(0, 128, 0));
		btnOtkai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnOtkai.setBounds(274, 357, 155, 50);
		getContentPane().add(btnOtkai);
		
		dateChooser = new JDateChooser();
		dateChooser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateChooser.setDateFormatString("dd.MM.yyyy.");
		dateChooser.setBounds(204, 196, 141, 23);
		getContentPane().add(dateChooser);
		dateChooser.setDate(new Date());
	}
	private void checkData() {
		String name = nametxt.getText();
		String surname = surnametxt.getText();
		String username = usernametxt.getText();
		String password = passwordtxt.getText();
		Gender gender;
		if (maleRadio.isSelected())gender = Gender.MALE;
		else gender = Gender.FEMALE;
		String phoneNumber = phoneNumbertxt.getText();
		String address = addresstxt.getText();
		String lbo = lbotxt.getText();
		
		if (name.equals("") || surname.equals("") || username.equals("") || password.equals("") || phoneNumber.equals("") || address.equals("") || lbo.equals("")) {
			JOptionPane.showMessageDialog(null, "Morate uneti sve podatke!", "Greška!", JOptionPane.ERROR_MESSAGE);
		}
		else if (this.managers.userExists(username)) {
			JOptionPane.showMessageDialog(null, "Uneto Korisničko ime već postoji, probajte sa drugim!", "Greška!", JOptionPane.ERROR_MESSAGE);
		}
		else if (dateChooser.getDate() == null) {
			JOptionPane.showMessageDialog(null, "Pogrešan datum!", "Greška!", JOptionPane.ERROR_MESSAGE);
		}
		else{
			createPatient(name, surname, username, password, gender, phoneNumber, address, lbo);
		}
	}
	
	private void createPatient(String name, String surname, String username, String password, Gender gender,
			String phoneNumber, String address, String lbo) {
		try {
			String stringDate = new SimpleDateFormat("dd.MM.yyyy.").format(dateChooser.getDate());
			LocalDate birthDate = LocalDate.parse(stringDate, Format.date_format);
			managers.getPatientMng().add(name, surname, username, password, gender, birthDate, phoneNumber, address, lbo);
			JOptionPane.showMessageDialog(null, "Uspešno upisan pacijent", "Uspešno upisan", JOptionPane.INFORMATION_MESSAGE);
			setVisible(false);
			dispose();
		} catch (DateTimeParseException e) {
			JOptionPane.showMessageDialog(null, "Pogrešan datum!", "Greška!", JOptionPane.ERROR_MESSAGE);
		}
	}
}
