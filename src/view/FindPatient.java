package view;


import javax.swing.JButton;
import javax.swing.JDialog;

import manage.ManagerFactory;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import entity.Patient;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FindPatient extends JDialog {
	
	private static final long serialVersionUID = -1795764553978105597L;
	private ManagerFactory managers;
	private JTextField lbotxt;
	private JLabel emptytxt;

	public FindPatient(ManagerFactory managers) {
		this.managers = managers;
		initialize();
	}
	public void initialize() {
		setTitle("Izbor pacijenta");
		setModal(true);
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setBounds(100, 100, 430, 205);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("LBO:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(76, 60, 46, 14);
		getContentPane().add(lblNewLabel);
		
		lbotxt = new JTextField();
		lbotxt.setBounds(124, 56, 180, 26);
		getContentPane().add(lbotxt);
		lbotxt.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Unesite LBO pacijenta");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(76, 11, 239, 26);
		getContentPane().add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Dalje");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String LBO = lbotxt.getText();
				if (LBO.equals("")) {
					emptytxt.setVisible(true);
				}
				else {
					Patient patient = managers.getPatientMng().findPatientByLbo(LBO);
					if (patient == null) {
						JOptionPane.showMessageDialog(null, "Ne postoji pacijent sa datim LBO-m", "Greška!", JOptionPane.ERROR_MESSAGE);
					}
					else {
						JOptionPane.showMessageDialog(null, "Pacijent"+patient.getName()+" "+patient.getSurname(), "Uspešno nađen pacijent", JOptionPane.INFORMATION_MESSAGE);
						setVisible(false);
						dispose();
						CreateRequest cr = new CreateRequest(managers, patient);
						cr.setVisible(true);
					}
				}
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(147, 93, 115, 40);
		getContentPane().add(btnNewButton);
		
		emptytxt = new JLabel("*");
		emptytxt.setVisible(false);
		emptytxt.setForeground(new Color(255, 0, 0));
		emptytxt.setFont(new Font("Tahoma", Font.BOLD, 20));
		emptytxt.setBounds(313, 56, 46, 26);
		getContentPane().add(emptytxt);
	}
}
