package view;


import javax.swing.JDialog;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.UIManager;

import entity.LabTest;
import manage.ManagerFactory;

import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SpinnerNumberModel;

public class InputLabTestPrice extends JDialog {
	
	private static final long serialVersionUID = 2944174204486507840L;
	private ManagerFactory managers;
	private LabTest labTest;
	private JSpinner priceSpinner;
	
	public InputLabTestPrice(ManagerFactory managers, LabTest labTest) {
		this.managers = managers;
		this.labTest = labTest;
		initialize();
	}
	public void initialize() {
		setTitle("Unos nove cene");
		getContentPane().setBackground(Color.LIGHT_GRAY);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nova cena");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(83, 45, 75, 14);
		getContentPane().add(lblNewLabel);
		
		priceSpinner = new JSpinner();
		priceSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(0), null, new Integer(1)));
		priceSpinner.setFont(new Font("Tahoma", Font.PLAIN, 14));
		priceSpinner.setBounds(168, 44, 93, 20);
		getContentPane().add(priceSpinner);
		
		JButton btnNewButton = new JButton("Postavi");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int price = (int) priceSpinner.getValue();
				managers.getLabtestMng().edit(labTest.getId(), labTest.getGroup(), labTest.getName(), price, labTest.getUnit(), labTest.getMinimum_value(), labTest.getMaximum_value());
				JOptionPane.showMessageDialog(null, "Uspe�no ste izmenili cenu!", "Uspe�no izmenjeno!", JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				dispose();
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setBackground(UIManager.getColor("Button.background"));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(63, 100, 142, 50);
		getContentPane().add(btnNewButton);
		
		JButton btnOtkai = new JButton("Otka\u017Ei");
		btnOtkai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnOtkai.setForeground(new Color(0, 128, 0));
		btnOtkai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnOtkai.setBackground(SystemColor.menu);
		btnOtkai.setBounds(237, 100, 142, 50);
		getContentPane().add(btnOtkai);
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 422, 214);
		
		setPriceValue();
	}
	private void setPriceValue() {
		priceSpinner.setValue(this.labTest.getPrice());
	}
}
