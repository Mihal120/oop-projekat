package view;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import entity.LabTechnician;
import entity.MedicalTechnician;
import entity.Patient;
import entity.User;
import manage.ManagerFactory;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.UIManager;

public class MainFrame extends JFrame {
	
	private static final long serialVersionUID = -327411445365812950L;
	private ManagerFactory managers;
	private User loggedUser;
	private JMenuItem unmeasuredResultsMenu;
	private JMenuItem analysisStatisticsMenu;
	private JMenuItem createPatientMenu;
	private JMenuItem notTakenRequestsMenu;
	private JMenuItem priceChangeMenu;
	private JMenuItem salaryBonusMenu;
	private JMenuItem incomeReportMenu;
	private JMenuItem patientReportMenu;
	private JMenuItem testDiscountsMenu;
	private JMenuItem changeLabTestPricesMenu;
	private JMenuItem patientRequestMenu;
	private JMenuItem createRequestMenu;
	private JLabel nametxt;
	private JLabel roletxt;
	
	public MainFrame(ManagerFactory managers, User loggedUser) {
		setResizable(false);
		setTitle("Labi laboratorija");
		this.managers = managers;
		this.loggedUser = loggedUser;
		initMainGUI();
	}
	
	
	public void initMainGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 554, 424);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Pregled");
		mnNewMenu.setBackground(new Color(128, 0, 0));
		menuBar.add(mnNewMenu);
		
		patientRequestMenu = new JMenuItem("Nalaza");
		patientRequestMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Patient patient= null;
				if (loggedUser instanceof Patient) {
					patient = (Patient) loggedUser;
				}
				PatientRequestsView pr = new PatientRequestsView(managers, patient);
				pr.setVisible(true);
			}
		});
		
		unmeasuredResultsMenu = new JMenuItem("Neizmerenih zahteva");
		mnNewMenu.add(unmeasuredResultsMenu);
		unmeasuredResultsMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LabTechnician labTech;
				if (loggedUser instanceof LabTechnician) {
					labTech = (LabTechnician) loggedUser;
					UnmeasuredResults ur = new UnmeasuredResults(managers, labTech);
					ur.setVisible(true);
				}
			}
		});
		mnNewMenu.add(patientRequestMenu);
		
		notTakenRequestsMenu = new JMenuItem("Ne uzetih nalaza");
		mnNewMenu.add(notTakenRequestsMenu);
		notTakenRequestsMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MedicalTechnician technician = null;
				if (loggedUser instanceof MedicalTechnician) {
					technician = (MedicalTechnician) loggedUser;
				}
				NotTakenRequests ntr = new NotTakenRequests(managers, technician);
				ntr.setVisible(true);
			}
		});
		
		JMenu mnNewMenu_2 = new JMenu("Zahtev");
		mnNewMenu_2.setBackground(new Color(128, 0, 0));
		menuBar.add(mnNewMenu_2);
		
		createPatientMenu = new JMenuItem("Registracija pacijenta");
		createPatientMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CreatePatient cp = new CreatePatient(managers);
				cp.setVisible(true);
			}
		});
		
		createRequestMenu = new JMenuItem("Zahtev za analizu");
		mnNewMenu_2.add(createRequestMenu);
		createRequestMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (loggedUser instanceof Patient) {
					Patient patient = (Patient) loggedUser;
					CreateRequest cr = new CreateRequest(managers, patient);
					cr.setVisible(true);
				}
				else {
					FindPatient fp = new FindPatient(managers);
					fp.setVisible(true);
				}
			}
		});
		mnNewMenu_2.add(createPatientMenu);
		
		JMenu mnNewMenu_3 = new JMenu("Izmena");
		mnNewMenu_3.setBackground(new Color(128, 0, 0));
		menuBar.add(mnNewMenu_3);
		
		priceChangeMenu = new JMenuItem("koeficijenta plata");
		priceChangeMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PriceChange pc = new PriceChange(managers.getPricesMng());
				pc.setVisible(true);
			}
		});
		mnNewMenu_3.add(priceChangeMenu);
		
		salaryBonusMenu = new JMenuItem("bonusa radnicima");
		salaryBonusMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SalaryBonus sb = new SalaryBonus(managers);
				sb.setVisible(true);
			}
		});
		mnNewMenu_3.add(salaryBonusMenu);
		
		testDiscountsMenu = new JMenuItem("popust na analize");
		mnNewMenu_3.add(testDiscountsMenu);
		
		changeLabTestPricesMenu = new JMenuItem("cena analiza");
		mnNewMenu_3.add(changeLabTestPricesMenu);
		changeLabTestPricesMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChangeLabTestPrices changePrices = new ChangeLabTestPrices(managers);
				changePrices.setVisible(true);
			}
		});
		testDiscountsMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TestDiscounts td = new TestDiscounts(managers);
				td.setVisible(true);
			}
		});
		
		JMenu mnNewMenu_4 = new JMenu("Izve\u0161taj");
		menuBar.add(mnNewMenu_4);
		
		incomeReportMenu = new JMenuItem("Prihodi i Rashodi");
		mnNewMenu_4.add(incomeReportMenu);
		
		patientReportMenu = new JMenuItem("Pacijenti");
		patientReportMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PatientReport pr = new PatientReport(managers);
				pr.setVisible(true);
			}
		});
		mnNewMenu_4.add(patientReportMenu);
		
		analysisStatisticsMenu = new JMenuItem("Statisti\u010Dka obrada");
		mnNewMenu_4.add(analysisStatisticsMenu);
		analysisStatisticsMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AnalysisStatistics as = new AnalysisStatistics(managers);
				as.setVisible(true);
			}
		});
		incomeReportMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IncomeReport r = new IncomeReport(managers);
				r.setVisible(true);
			}
		});
		JPanel contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		BufferedImage myPicture = null;
		try {
			myPicture = ImageIO.read(new File("./data/userIcon.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		JLabel lblNewLabel = new JLabel(new ImageIcon(myPicture));
		lblNewLabel.setBounds(201, 30, 150, 150);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Ime i prezime:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setBounds(164, 198, 109, 30);
		contentPane.add(lblNewLabel_1);
		
		nametxt = new JLabel("New label");
		nametxt.setFont(new Font("Tahoma", Font.BOLD, 14));
		nametxt.setBounds(272, 203, 124, 20);
		contentPane.add(nametxt);
		
		JLabel lblNewLabel_2 = new JLabel("Uloga:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_2.setBounds(218, 239, 61, 30);
		contentPane.add(lblNewLabel_2);
		
		roletxt = new JLabel("Uloga:");
		roletxt.setFont(new Font("Tahoma", Font.BOLD, 14));
		roletxt.setBounds(272, 239, 160, 30);
		contentPane.add(roletxt);
		
		JButton btnNewButton = new JButton("Odjavi se");
		btnNewButton.setBackground(UIManager.getColor("Button.darkShadow"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
				LoginGUI lg = new LoginGUI(managers);
				lg.setVisible(true);
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(201, 280, 150, 53);
		contentPane.add(btnNewButton);
		
		
		
		setAvailability();
		
	}


	private void setAvailability() {
		if (this.loggedUser instanceof Patient) {
			this.analysisStatisticsMenu.setEnabled(false);
			this.changeLabTestPricesMenu.setEnabled(false);
			this.createPatientMenu.setEnabled(false);
			this.incomeReportMenu.setEnabled(false);
			this.notTakenRequestsMenu.setEnabled(false);
			this.patientReportMenu.setEnabled(false);
			this.priceChangeMenu.setEnabled(false);
			this.unmeasuredResultsMenu.setEnabled(false);
			this.salaryBonusMenu.setEnabled(false);
			this.testDiscountsMenu.setEnabled(false);
			this.nametxt.setText(this.loggedUser.getName()+" "+this.loggedUser.getSurname());
			this.roletxt.setText("Pacijent");
		}
		else if (this.loggedUser instanceof LabTechnician) {
			this.patientRequestMenu.setEnabled(false);
			this.createRequestMenu.setEnabled(false);
			this.changeLabTestPricesMenu.setEnabled(false);
			this.createPatientMenu.setEnabled(false);
			this.incomeReportMenu.setEnabled(false);
			this.notTakenRequestsMenu.setEnabled(false);
			this.patientReportMenu.setEnabled(false);
			this.priceChangeMenu.setEnabled(false);
			this.salaryBonusMenu.setEnabled(false);
			this.testDiscountsMenu.setEnabled(false);
			this.nametxt.setText(this.loggedUser.getName()+" "+this.loggedUser.getSurname());
			this.roletxt.setText("Laborant");
		}
		else if (this.loggedUser instanceof MedicalTechnician) {
			this.analysisStatisticsMenu.setEnabled(false);
			this.changeLabTestPricesMenu.setEnabled(false);
			this.incomeReportMenu.setEnabled(false);
			this.patientReportMenu.setEnabled(false);
			this.priceChangeMenu.setEnabled(false);
			this.unmeasuredResultsMenu.setEnabled(false);
			this.salaryBonusMenu.setEnabled(false);
			this.testDiscountsMenu.setEnabled(false);
			this.nametxt.setText(this.loggedUser.getName()+" "+this.loggedUser.getSurname());
			this.roletxt.setText("Medicinski tehničar");
		}
		else {
			this.nametxt.setText(this.loggedUser.getName()+" "+this.loggedUser.getSurname());
			this.roletxt.setText("Administrator");
		}
		
	}
}
