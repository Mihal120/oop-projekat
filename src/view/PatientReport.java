package view;


import javax.swing.JDialog;
import java.awt.Color;
import com.toedter.calendar.JDateChooser;

import manage.ManagerFactory;
import tableModel.PatientTableModel;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PatientReport extends JDialog {
	
	private static final long serialVersionUID = -7620702692091241360L;
	private ManagerFactory managers;
	private JTable table;
	private JDateChooser dateFromChooser;
	private JDateChooser dateToChooser;
	private JComboBox<String> groupBox;
	private TableRowSorter<AbstractTableModel> tableSorter = new TableRowSorter<AbstractTableModel>();
	
	public PatientReport(ManagerFactory managers) {
		this.managers = managers;
		initialize();
	}
	public void initialize() {
		setTitle("Izve\u0161taj za pacijente");
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 857, 404);
		getContentPane().setLayout(null);
		
		dateFromChooser = new JDateChooser();
		dateFromChooser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateFromChooser.setDateFormatString("dd.MM.yyyy.");
		dateFromChooser.setBounds(101, 11, 113, 20);
		getContentPane().add(dateFromChooser);
		
		dateToChooser = new JDateChooser();
		dateToChooser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateToChooser.setDateFormatString("dd.MM.yyyy.");
		dateToChooser.setBounds(306, 11, 113, 20);
		getContentPane().add(dateToChooser);
		
		JLabel lblNewLabel = new JLabel("Od");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(57, 11, 34, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Do");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(261, 11, 46, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1_3 = new JLabel("Grupe analiza");
		lblNewLabel_1_1_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_3.setBounds(445, 11, 95, 14);
		getContentPane().add(lblNewLabel_1_1_3);
		
		groupBox = new JComboBox<String>();
		groupBox.setBounds(547, 11, 112, 20);
		getContentPane().add(groupBox);
		
		JButton btnNewButton = new JButton("Prika\u017Ei");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				changeTables();
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(702, 11, 89, 23);
		getContentPane().add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 46, 831, 263);
		getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		addGroupBoxValues();
	}
	
	private void addGroupBoxValues() {
		ArrayList<String> groups = this.managers.getLabtestMng().getGroups();
		for (String group : groups) {
			groupBox.addItem(group);
		}
	}
	
	private void changeTables() {
		Date fromDate = dateFromChooser.getDate();
		Date toDate = dateToChooser.getDate();
		if (fromDate == null || toDate == null) {
			JOptionPane.showMessageDialog(null, "Izaberite datume!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
		}
		else {
			String group = (String) groupBox.getSelectedItem();
			PatientTableModel tableModel = new PatientTableModel(managers, fromDate, toDate, group);
			if (tableModel.getRows().size() == 0) {
				JOptionPane.showMessageDialog(null, "Nema zahteva u zadatom vremenu!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
			}
			else {
				tableSorter.setModel(tableModel);
				table.setModel(tableModel);
				table.setRowSorter(tableSorter);
			}
		}
	}
}
