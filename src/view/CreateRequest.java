package view;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JCheckBox;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import entity.Format;
import entity.LabTest;
import entity.MedicalResult;
import entity.Patient;
import manage.ManagerFactory;
import tableModel.LabTestTableModel;

import javax.swing.ListSelectionModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import com.toedter.calendar.JDateChooser;

public class CreateRequest extends JDialog {
	
	private static final long serialVersionUID = 9107966362719716360L;
	private Patient patient;
	private ManagerFactory managers;
	private TableRowSorter<AbstractTableModel> tableSorter = new TableRowSorter<AbstractTableModel>();
	private final JPanel contentPanel = new JPanel();
	private JTable leftTable;
	private JTable rightTable;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton radioOrdination;
	private JRadioButton radioHome;
	private JSpinner timeSpinner;
	private JLabel pricetxt;
	private JCheckBox timeCheck;
	private JDateChooser dateChooser;
	
	public CreateRequest(ManagerFactory managers, Patient patient) {
		this.patient = patient;
		this.managers = managers;
		initialize();
	}
		
	public void initialize() {
		LabTestTableModel lefttableModel = new LabTestTableModel(this.managers.getLabtestMng());
		LabTestTableModel righttableModel = new LabTestTableModel();
		setTitle("Kreiranje zahteva");
		setBounds(100, 100, 684, 481);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.LIGHT_GRAY);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 26, 296, 203);
		contentPanel.add(scrollPane);
		
		leftTable = new JTable(lefttableModel);
		leftTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableSorter.setModel((AbstractTableModel) leftTable.getModel());
		leftTable.setRowSorter(tableSorter);
		scrollPane.setViewportView(leftTable);
		
		JLabel lblNewLabel = new JLabel("Na\u010Din uzimanja uzorka:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(119, 240, 157, 14);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Selektujte analize koje \u017Eelite da radite u tabeli");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(10, 11, 282, 14);
		contentPanel.add(lblNewLabel_1);
		
		radioHome = new JRadioButton("Ku\u0107na poseta");
		radioHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changePrice();
			}
		});
		radioHome.setFont(new Font("Tahoma", Font.PLAIN, 13));
		buttonGroup.add(radioHome);
		radioHome.setBackground(Color.LIGHT_GRAY);
		radioHome.setBounds(391, 238, 109, 23);
		contentPanel.add(radioHome);
		
		radioOrdination = new JRadioButton("U ordinaciji");
		radioOrdination.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				changePrice();
			}
		});
		radioOrdination.setFont(new Font("Tahoma", Font.PLAIN, 13));
		radioOrdination.setSelected(true);
		buttonGroup.add(radioOrdination);
		radioOrdination.setBackground(Color.LIGHT_GRAY);
		radioOrdination.setBounds(282, 238, 109, 23);
		contentPanel.add(radioOrdination);
		
		JLabel lblNewLabel_2 = new JLabel("Datum:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(119, 265, 134, 23);
		contentPanel.add(lblNewLabel_2);
		
		timeSpinner = new JSpinner();
		timeSpinner.setEnabled(false);
		timeSpinner.setFont(new Font("Tahoma", Font.PLAIN, 14));
		timeSpinner.setModel(new SpinnerDateModel(new Date(1597269600000L), null, null, Calendar.HOUR_OF_DAY));
		JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(timeSpinner, "HH:mm");
		timeSpinner.setEditor(timeEditor);
		timeSpinner.setBounds(326, 302, 65, 20);
		contentPanel.add(timeSpinner);
		
		timeCheck = new JCheckBox("\u017Delim odre\u0111eno vreme");
		timeCheck.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (timeCheck.isSelected()) {
					timeSpinner.setEnabled(true);
					float price = Float.parseFloat(pricetxt.getText().split(" ")[0]);
					price += managers.getPricesMng().getPrices().getTimePrice();
					pricetxt.setText(price+" din.");
				}
				else {
					timeSpinner.setEnabled(false);
					float price = Float.parseFloat(pricetxt.getText().split(" ")[0]);
					price -= managers.getPricesMng().getPrices().getTimePrice();
					pricetxt.setText(price+" din.");
				}
			}
		});
		timeCheck.setFont(new Font("Tahoma", Font.PLAIN, 14));
		timeCheck.setBackground(Color.LIGHT_GRAY);
		timeCheck.setBounds(129, 301, 173, 23);
		contentPanel.add(timeCheck);
		
		JButton btnNewButton = new JButton("Potvrdi");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean homeArrival = radioHome.isSelected();
				ArrayList<LabTest> tests = righttableModel.getRows();
				if (tests.size() == 0) {
					JOptionPane.showMessageDialog(null, "Morate izabrati barem jednu analizu!", "Greška!", JOptionPane.ERROR_MESSAGE);
				}
				else createRequest(homeArrival, tests);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setForeground(new Color(0, 100, 0));
		btnNewButton.setBounds(164, 381, 152, 50);
		contentPanel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Otka\u017Ei");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		btnNewButton_1.setForeground(new Color(0, 100, 0));
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_1.setBounds(354, 381, 146, 50);
		contentPanel.add(btnNewButton_1);
		
		JLabel lblNewLabel_3 = new JLabel("Napomena: ta\u010Dno vreme se napla\u0107uje");
		lblNewLabel_3.setForeground(new Color(255, 0, 0));
		lblNewLabel_3.setBounds(401, 302, 229, 23);
		contentPanel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Ukupna cena:");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_4.setBounds(174, 331, 118, 28);
		contentPanel.add(lblNewLabel_4);
		
		pricetxt = new JLabel("0 din.");
		pricetxt.setFont(new Font("Tahoma", Font.BOLD, 14));
		pricetxt.setBounds(292, 333, 99, 23);
		contentPanel.add(pricetxt);
		
		
		JButton btnNewButton_2 = new JButton("Dodaj ->");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int index = leftTable.convertRowIndexToModel(leftTable.getSelectedRow());
					LabTest lt = managers.getLabtestMng().getLabTests().get(index);
					if (righttableModel.rowExists(lt)) {
						JOptionPane.showMessageDialog(null, "Već ste izabrali tu analizu!", "Greška!", JOptionPane.ERROR_MESSAGE);
					}
					else {
						righttableModel.addRow(lt);
					}					
				} catch (IndexOutOfBoundsException e) {
					JOptionPane.showMessageDialog(null, "Morate izabrati neku analizu u tabeli!", "Greška!", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton_2.setBounds(306, 85, 85, 23);
		contentPanel.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("<- Izbaci");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					righttableModel.removeRow(rightTable.getSelectedRow());
				} catch (IndexOutOfBoundsException e) {
					JOptionPane.showMessageDialog(null, "Morate izabrati neku analizu u tabeli!", "Greška!", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		btnNewButton_3.setBounds(306, 135, 85, 23);
		contentPanel.add(btnNewButton_3);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(390, 26, 268, 203);
		contentPanel.add(scrollPane_1);
		
		rightTable = new JTable(righttableModel);
		rightTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		rightTable.getModel().addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent e) {
				//#ovde dodati za popust za analize i za pacijenta
				float price = 0;
				for (LabTest test : righttableModel.getRows()) {
					price += test.getPrice();
				}
				pricetxt.setText(price+" din.");
			}
		});
		scrollPane_1.setViewportView(rightTable);
		
		JLabel lblNewLabel_5 = new JLabel("Analize koje želite da radite");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_5.setBounds(391, 11, 199, 14);
		contentPanel.add(lblNewLabel_5);
		
		dateChooser = new JDateChooser();
		dateChooser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateChooser.setDateFormatString("dd.MM.yyyy.");
		dateChooser.setBounds(180, 265, 136, 23);
		contentPanel.add(dateChooser);
		dateChooser.setDate(new Date());
		
		checkLastResults(righttableModel);
	}

	private void checkLastResults(LabTestTableModel tableModel) {
		ArrayList<MedicalResult> results = managers.getRequestMng().getOutOfRangePatientResults(patient);
		if (results.size() > 0) {
			String text = "";
			for (MedicalResult medicalResult : results) {
				text += medicalResult.getTestType().getName()+", ";
			}
			int dialogResult = JOptionPane.showConfirmDialog (null, "U prethodna dva nalaza ste imali povišenu izmerenu vrednost "+text+" Da li želite da ih dodate kao željene nalaze?","Pitanje",JOptionPane.YES_NO_OPTION);
			if(dialogResult == JOptionPane.YES_OPTION){
				for (MedicalResult medicalResult : results) {
					tableModel.addRow(medicalResult.getTestType());
				}
			}
		}
	}

	protected void changePrice() {
		if (radioHome.isSelected()) {
			float price = Float.parseFloat(pricetxt.getText().split(" ")[0]);
			price += managers.getPricesMng().getPrices().getHomeArrivalPrice();
			pricetxt.setText(price+" din.");
		}
		else {
			float price = Float.parseFloat(pricetxt.getText().split(" ")[0]);
			price -= managers.getPricesMng().getPrices().getHomeArrivalPrice();
			pricetxt.setText(price+" din.");
		}
		
	}

	private void createRequest(boolean homeArrival, ArrayList<LabTest> tests) {
		LocalDateTime takenDateTime = getDateTime();
		if (takenDateTime.isBefore(LocalDateTime.now())) {
			JOptionPane.showMessageDialog(null, "Datum i vreme ne može da budu pre sada!", "Greška!", JOptionPane.ERROR_MESSAGE);
		}
		else {
			for (LabTest labTest : tests) {
				if (managers.getTestdiscountMng().findTestDiscount(labTest, Date.from(takenDateTime.atZone(ZoneId.systemDefault()).toInstant())) > 0) {
					JOptionPane.showMessageDialog(null, "Imate popust na određene analize, biće vam uračunat kad nalaz bude završen!", "Popust!", JOptionPane.INFORMATION_MESSAGE);					
				}
			}
			managers.getRequestMng().add(patient, homeArrival, takenDateTime, tests);
			JOptionPane.showMessageDialog(null, "Uspešno ste napravili zahtev!", "Uspešno poslato!", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	private LocalDateTime getDateTime() {
		String stringDate = new SimpleDateFormat("dd.MM.yyyy. HH:mm").format(dateChooser.getDate());
		if (timeCheck.isSelected()) {
			String stringTime = new SimpleDateFormat("HH:mm").format(timeSpinner.getValue());
			LocalDate takenDate = LocalDate.parse(stringDate, Format.date_format);
			LocalTime takenTime = LocalTime.parse(stringTime, DateTimeFormatter.ofPattern("HH:mm"));
			return LocalDateTime.of(takenDate, takenTime);
		}
		else {
			return LocalDateTime.parse(stringDate, Format.date_time_format);
		}
	}
}
