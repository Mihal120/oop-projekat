package view;


import javax.swing.JDialog;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JSpinner;

import entity.Employee;
import entity.LabTechnician;
import entity.MedicalTechnician;
import manage.ManagerFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SpinnerNumberModel;

public class ChangeBonusSalary extends JDialog {
	private static final long serialVersionUID = 1451596277740132365L;
	private Employee employee;
	private ManagerFactory managers;
	private JLabel bonuslbl;
	private JSpinner bonusSpinner;

	public ChangeBonusSalary(Employee employee, ManagerFactory managers) {
		setTitle("Izmena bonusa");
		this.employee = employee;
		this.managers = managers;
		initialize();
	}
	public void initialize() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bonus:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(80, 46, 61, 31);
		getContentPane().add(lblNewLabel);
		
		bonusSpinner = new JSpinner();
		bonusSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		bonusSpinner.setFont(new Font("Tahoma", Font.PLAIN, 14));
		bonusSpinner.setBounds(167, 50, 135, 24);
		getContentPane().add(bonusSpinner);
		
		bonuslbl = new JLabel("New label");
		bonuslbl.setForeground(new Color(128, 0, 0));
		bonuslbl.setFont(new Font("Tahoma", Font.PLAIN, 14));
		bonuslbl.setBounds(53, 11, 301, 24);
		getContentPane().add(bonuslbl);
		
		JButton btnNewButton = new JButton("Sa\u010Duvaj");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int bonus = (int)bonusSpinner.getValue();
				if (employee instanceof LabTechnician) {
					LabTechnician lt = (LabTechnician) employee;
					managers.getLabtechMng().edit(lt.getId(), lt.getName(), lt.getSurname(),lt.getUsername(),lt.getPassword(), lt.getGender(), lt.getBirth_date(), lt.getPhone_number(), lt.getAddress(), lt.getQualification(), lt.getInternship(), lt.getBasis_salary(), bonus, lt.getSpecializations());	
				}
				else {
					MedicalTechnician mt = (MedicalTechnician) employee;
					managers.getMedtechMng().edit(mt.getId(), mt.getName(), mt.getSurname(),mt.getUsername(),mt.getPassword(), mt.getGender(), mt.getBirth_date(), mt.getPhone_number(), mt.getAddress(), mt.getQualification(), mt.getInternship(), mt.getBasis_salary(), bonus);
				}
				JOptionPane.showMessageDialog(null, "Uspešno izmenjeni podaci", "Uspešno izmenjeno", JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				dispose();
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(53, 88, 144, 46);
		getContentPane().add(btnNewButton);
		
		JButton btnOtkai = new JButton("Otka\u017Ei");
		btnOtkai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnOtkai.setForeground(new Color(0, 128, 0));
		btnOtkai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnOtkai.setBounds(227, 88, 144, 46);
		getContentPane().add(btnOtkai);
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 413, 188);
		
		setValues();
	}
	private void setValues() {
		if (this.employee instanceof LabTechnician)bonuslbl.setText("Bonus na specijalizaciju");
		else bonuslbl.setText("Bonus na kućnu posetu");
		bonusSpinner.setValue(this.employee.getBonus_salary());
	}

}
