package view;


import javax.swing.JDialog;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;

import entity.LabTechnician;
import entity.MedicalResult;
import manage.ManagerFactory;

import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Random;
import java.awt.event.ActionEvent;

public class InputResult extends JDialog {
	private static final long serialVersionUID = 5265246567531177394L;
	private MedicalResult result;
	private ManagerFactory managers;
	private LabTechnician labTech;
	private JLabel grouplbl;
	private JLabel namelbl;
	private JTextField valuetxt;
	private JLabel unitlbl;

	public InputResult(MedicalResult result, ManagerFactory managers, LabTechnician labTech) {
		this.result = result;
		this.managers = managers;
		this.labTech = labTech;
		initialize();
	}
	public void initialize() {
		setResizable(false);
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setTitle("Unos rezultata");
		setModal(true);
		setBounds(100, 100, 485, 420);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Grupa:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(72, 31, 69, 30);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNaziv = new JLabel("Naziv:");
		lblNaziv.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNaziv.setBounds(72, 72, 69, 30);
		getContentPane().add(lblNaziv);
		
		grouplbl = new JLabel("lbl");
		grouplbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		grouplbl.setBounds(151, 31, 101, 30);
		getContentPane().add(grouplbl);
		
		namelbl = new JLabel("lbl");
		namelbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		namelbl.setBounds(151, 72, 101, 30);
		getContentPane().add(namelbl);
		
		JLabel lblIzmerenaVrednost = new JLabel("Izmerena vrednost:");
		lblIzmerenaVrednost.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIzmerenaVrednost.setBounds(34, 126, 131, 30);
		getContentPane().add(lblIzmerenaVrednost);
		
		valuetxt = new JTextField();
		valuetxt.setBounds(163, 132, 112, 23);
		getContentPane().add(valuetxt);
		valuetxt.setColumns(10);
		
		JButton btnNewButton = new JButton("U\u010Ditaj iz ma\u0161ine");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				float min = (float) (result.getTestType().getMinimum_value() - 0.2*result.getTestType().getMinimum_value());
				float max = (float) (result.getTestType().getMaximum_value() + 0.2*result.getTestType().getMaximum_value());
				Random r = new Random();
				float randomValue = min + r.nextFloat() * (max - min);
				valuetxt.setText(String.valueOf(randomValue));
			}
		});
		btnNewButton.setForeground(new Color(0, 139, 139));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(297, 126, 143, 30);
		getContentPane().add(btnNewButton);
		
		JLabel lblJedinicaMere = new JLabel("Jedinica mere:");
		lblJedinicaMere.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblJedinicaMere.setBounds(34, 178, 107, 30);
		getContentPane().add(lblJedinicaMere);
		
		unitlbl = new JLabel("lbl");
		unitlbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		unitlbl.setBounds(151, 178, 101, 30);
		getContentPane().add(unitlbl);
		
		JButton btnNewButton_1 = new JButton("Unesi");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String value = valuetxt.getText();
				if (value.equals("")) {
					JOptionPane.showMessageDialog(null, "Morate uneti izmerenu vrednost!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
				}
				else {
					LocalDate date = LocalDate.now();
					try {
						float measuredValue = Float.parseFloat(value);
						result.setDate(date);
						result.setMeasured_value(measuredValue);
						managers.getMedresMng().edit(result.getId(), result.getTestType(), result.getMeasured_value(), result.getDate(), labTech);
						JOptionPane.showMessageDialog(null, "Uspe�no ste upisali izmerenu vrednost", "Uspe�no upisano", JOptionPane.INFORMATION_MESSAGE);
						managers.getRequestMng().checkForFinishedRequest(result);
						setVisible(false);
						dispose();						
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, "Morate uneti brojnu vrednost!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnNewButton_1.setForeground(new Color(0, 128, 0));
		btnNewButton_1.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_1.setBounds(66, 250, 127, 48);
		getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_1_1 = new JButton("Iza\u0111i");
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnNewButton_1_1.setForeground(new Color(0, 128, 0));
		btnNewButton_1_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_1_1.setBackground(SystemColor.menu);
		btnNewButton_1_1.setBounds(242, 250, 127, 48);
		getContentPane().add(btnNewButton_1_1);
		
		setValues();
	}
	private void setValues() {
		grouplbl.setText(this.result.getTestType().getGroup());
		namelbl.setText(this.result.getTestType().getName());
		unitlbl.setText(this.result.getTestType().getUnit());
	}

}
