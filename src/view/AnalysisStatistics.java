package view;


import javax.swing.JDialog;

import manage.ManagerFactory;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import com.toedter.calendar.JDateChooser;

import entity.LabTest;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.style.Styler.LegendPosition;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class AnalysisStatistics extends JDialog {
	
	private static final long serialVersionUID = -5432183107900523338L;
	private ManagerFactory managers;
	private JSpinner yearsToSpinner;
	private JSpinner yearsFromSpinner;
	private JDateChooser dateFromChooser;
	private JComboBox<String> analysisBox;
	private JDateChooser dateToChooser;
	private XChartPanel<XYChart> chartPane;
	private JPanel panel;
	
	public AnalysisStatistics(ManagerFactory managers) {
		this.managers = managers;
		initialize();
		
	}	
	public void initialize() {
			
		setBounds(100, 100, 886, 385);
		setTitle("Statistika obrade");
		getContentPane().setBackground(Color.LIGHT_GRAY);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Datum od:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(66, 30, 78, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblDo = new JLabel("do:");
		lblDo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDo.setBounds(280, 30, 78, 14);
		getContentPane().add(lblDo);
		
		dateFromChooser = new JDateChooser();
		dateFromChooser.setBounds(142, 28, 107, 20);
		getContentPane().add(dateFromChooser);
		
		dateToChooser = new JDateChooser();
		dateToChooser.setBounds(313, 28, 107, 20);
		getContentPane().add(dateToChooser);
		
		JLabel lblStarosnaGranica = new JLabel("Starosna granica od:");
		lblStarosnaGranica.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblStarosnaGranica.setBounds(66, 77, 142, 14);
		getContentPane().add(lblStarosnaGranica);
		
		yearsFromSpinner = new JSpinner();
		yearsFromSpinner.setModel(new SpinnerNumberModel(1, 1, 100, 1));
		yearsFromSpinner.setBounds(223, 74, 48, 25);
		getContentPane().add(yearsFromSpinner);
		
		yearsToSpinner = new JSpinner();
		yearsToSpinner.setModel(new SpinnerNumberModel(1, 1, 100, 1));
		yearsToSpinner.setBounds(342, 74, 48, 25);
		getContentPane().add(yearsToSpinner);
		
		JLabel lblDo_1 = new JLabel("do:");
		lblDo_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDo_1.setBounds(300, 79, 78, 14);
		getContentPane().add(lblDo_1);
		
		JButton btnNewButton = new JButton("Prika\u017Ei");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				checkValues();
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(66, 217, 135, 48);
		getContentPane().add(btnNewButton);
		
		JButton btnIzai = new JButton("Iza\u0111i");
		btnIzai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		btnIzai.setForeground(new Color(0, 128, 0));
		btnIzai.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnIzai.setBounds(272, 217, 135, 48);
		getContentPane().add(btnIzai);
		
		panel = new JPanel();
		panel.setBackground(Color.GRAY);
		panel.setBounds(453, 11, 417, 335);
		getContentPane().add(panel);
		
		analysisBox = new JComboBox<String>();
		analysisBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		analysisBox.setBounds(194, 132, 171, 25);
		getContentPane().add(analysisBox);
		
		JLabel lblAnaliza = new JLabel("Analiza:");
		lblAnaliza.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAnaliza.setBounds(95, 135, 89, 14);
		getContentPane().add(lblAnaliza);
		setResizable(false);
		setModal(true);
		
		addAnalysis();
	}
	protected void checkValues() {
		int yearFrom = (int) yearsFromSpinner.getValue();
		int yearTo = (int) yearsToSpinner.getValue();
		String analysis = (String) this.analysisBox.getSelectedItem();
		Date fromDate = this.dateFromChooser.getDate();
		Date toDate = this.dateToChooser.getDate();
		if (fromDate == null || toDate == null) {
			JOptionPane.showMessageDialog(null, "Morate uneti oba datuma!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
		}
		else if (fromDate.after(toDate)) {
			JOptionPane.showMessageDialog(null, "Prvi datum mora biti pre drugog!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
		}
		else if (yearFrom > yearTo) {
			JOptionPane.showMessageDialog(null, "Opseg godina mora da bude validan!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
		}
		else {
			LabTest lt = this.managers.getLabtestMng().getLabTestByName(analysis);
			createChart(fromDate, toDate, yearFrom, yearTo, lt);
		} 
	}
	
	
	private void createChart(Date fromDate, Date toDate, int fromYear, int toYear, LabTest test) {
		if (chartPane != null) {
			panel.remove(chartPane);
			panel.revalidate();
			validate();
			repaint();
		}
		XYChart chart = new XYChartBuilder().width(417).height(335).title("Statistika analiza").xAxisTitle("Datum").yAxisTitle("Vrednosti").build();
	 
	    chart.getStyler().setLegendPosition(LegendPosition.InsideNE);
	    chart.getStyler().setAxisTitlesVisible(false);
	    chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Area);
	    chart.getStyler().setDatePattern("dd.MM.yyyy.");
	    chart.getStyler().setDecimalPattern("#0.000");
	    
	    ArrayList<Date> xMaleData = new ArrayList<Date>();
	    ArrayList<Double> yMaleData = new ArrayList<Double>();
	    ArrayList<Date> xFemaleData = new ArrayList<Date>();
	    ArrayList<Double> yFemaleData = new ArrayList<Double>();
	    ArrayList<Date> xCombinedData = new ArrayList<Date>();
	    ArrayList<Double> yCombinedData = new ArrayList<Double>();
	    
	    this.managers.getRequestMng().getStatistics(xMaleData, yMaleData, fromDate, toDate, fromYear, toYear, test, "MALE");
	    this.managers.getRequestMng().getStatistics(xFemaleData, yFemaleData, fromDate, toDate, fromYear, toYear, test, "FEMALE");
	    this.managers.getRequestMng().getStatistics(xCombinedData, yCombinedData, fromDate, toDate, fromYear, toYear, test, "COMBINED");
	 
	    if (xMaleData.size() == 0 || xFemaleData.size() == 0 || xCombinedData.size() == 0) {
	    	JOptionPane.showMessageDialog(null, "Nema podataka za izabrane parametre!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
		}
	    else {
		    chart.addSeries("mu�ko", xMaleData, yMaleData);
		    chart.addSeries("�ensko", xFemaleData, yFemaleData);
		    chart.addSeries("kombinovano", xCombinedData, yCombinedData);
		    
		    chartPane = new XChartPanel<>(chart);
	        this.panel.add(chartPane);
	        panel.revalidate();
			validate();
			repaint();
	    }
	}
	
	private void addAnalysis() {
		ArrayList<String> allAnalysis = this.managers.getLabtestMng().getAnalysis();
		for (String analysis : allAnalysis) {
			this.analysisBox.addItem(analysis);
		}
		
	}

}
