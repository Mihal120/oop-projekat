package view;

import javax.swing.JDialog;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import com.toedter.calendar.JDateChooser;

import entity.LabTest;
import manage.ManagerFactory;
import tableModel.LabTestTableModel;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ItemEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class TestDiscounts extends JDialog {
	
	private static final long serialVersionUID = -7197536902148635154L;
	private ManagerFactory managers;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton DiscountOnGroup;
	private JRadioButton DiscountOnTest;
	private JDateChooser dateFromChooser;
	private JDateChooser dateToChooser;
	private JTable table;
	private JComboBox<String> groupBox;
	private JSpinner discountSpinner;
	private JComboBox<String> dayOfWeekBox;
	
	public TestDiscounts(ManagerFactory managers) {
		this.managers = managers;
		initialize();
	}
	public void initialize() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setResizable(false);
		setModal(true);
		setTitle("Popusti na analize");
		setBounds(100, 100, 577, 490);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Popust va\u017Ei od:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(77, 21, 106, 22);
		getContentPane().add(lblNewLabel);
		
		dateFromChooser = new JDateChooser();
		dateFromChooser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateFromChooser.setDateFormatString("dd.MM.yyyy.");
		dateFromChooser.setBounds(199, 23, 119, 20);
		getContentPane().add(dateFromChooser);
		
		JLabel lblDo = new JLabel("Do:");
		lblDo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDo.setBounds(351, 21, 36, 22);
		getContentPane().add(lblDo);
		
		dateToChooser = new JDateChooser();
		dateToChooser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateToChooser.setDateFormatString("dd.MM.yyyy.");
		dateToChooser.setBounds(397, 23, 119, 20);
		getContentPane().add(dateToChooser);
		
		DiscountOnGroup = new JRadioButton("Popust na grupu analiza");
		
		DiscountOnGroup.setSelected(true);
		buttonGroup.add(DiscountOnGroup);
		DiscountOnGroup.setBackground(Color.LIGHT_GRAY);
		DiscountOnGroup.setFont(new Font("Tahoma", Font.PLAIN, 14));
		DiscountOnGroup.setBounds(113, 103, 171, 23);
		getContentPane().add(DiscountOnGroup);
		
		DiscountOnTest = new JRadioButton("Popust na odre\u0111enu analizu");
		buttonGroup.add(DiscountOnTest);
		DiscountOnTest.setFont(new Font("Tahoma", Font.PLAIN, 14));
		DiscountOnTest.setBackground(Color.LIGHT_GRAY);
		DiscountOnTest.setBounds(328, 105, 195, 23);
		getContentPane().add(DiscountOnTest);
		
		JLabel lblGrupaAnaliza = new JLabel("Grupa analiza:");
		lblGrupaAnaliza.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGrupaAnaliza.setBounds(58, 65, 85, 22);
		getContentPane().add(lblGrupaAnaliza);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 139, 546, 199);
		getContentPane().add(scrollPane);
		table = new JTable();
		scrollPane.setViewportView(table);
		
		groupBox = new JComboBox<String>();
		//addGroupBoxValues();
		groupBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				setTableValues();
			}
		});
		groupBox.setBounds(153, 68, 112, 20);
		getContentPane().add(groupBox);
		
		
		DiscountOnGroup.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (DiscountOnGroup.isSelected()) {
					table.setEnabled(false);
				}
				else {
					table.setEnabled(true);
				}
			}
		});
		JButton btnNewButton = new JButton("Postavi popust");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				checkDates();
			}
		});
		btnNewButton.setForeground(new Color(0, 128, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(94, 391, 154, 37);
		getContentPane().add(btnNewButton);
		
		JButton btnIzadji = new JButton("Izadji");
		btnIzadji.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnIzadji.setForeground(new Color(0, 128, 0));
		btnIzadji.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnIzadji.setBounds(292, 391, 154, 37);
		getContentPane().add(btnIzadji);
		
		JLabel lblPopust = new JLabel("Popust:");
		lblPopust.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPopust.setBounds(148, 345, 85, 22);
		getContentPane().add(lblPopust);
		
		discountSpinner = new JSpinner();
		discountSpinner.setFont(new Font("Tahoma", Font.PLAIN, 14));
		discountSpinner.setModel(new SpinnerNumberModel(5, 5, 100, 5));
		discountSpinner.setBounds(227, 348, 57, 19);
		getContentPane().add(discountSpinner);
		
		JLabel lblPopust_1 = new JLabel("%");
		lblPopust_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPopust_1.setBounds(287, 345, 21, 22);
		getContentPane().add(lblPopust_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Dan u nedelji");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1.setBounds(292, 67, 95, 14);
		getContentPane().add(lblNewLabel_1_1);
		
		dayOfWeekBox = new JComboBox<String>();
		dayOfWeekBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Ponedeljak", "Utorak", "Sreda", "\u010Cetvrtak", "Petak", "Subota", "Nedelja"}));
		dayOfWeekBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		dayOfWeekBox.setBounds(383, 65, 132, 23);
		getContentPane().add(dayOfWeekBox);
		
		addGroupBoxValues();
	}
	
	protected void checkDates() {
		Date fromDate = dateFromChooser.getDate();
		Date toDate = dateToChooser.getDate();
		String group = (String) groupBox.getSelectedItem();
		int dayOfWeek = dayOfWeekBox.getSelectedIndex();
		float discount = (float)((int)discountSpinner.getValue())/100;
		if (fromDate == null || toDate == null) {
			JOptionPane.showMessageDialog(null, "Izaberite datume!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
		}
		else if (fromDate.after(toDate)) {
			JOptionPane.showMessageDialog(null, "Prvi datum mora biti pre drugog!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
		} 
		else if (DiscountOnGroup.isSelected()) {
			LabTest test = null;
			this.managers.getTestdiscountMng().add(group, test, discount, fromDate, toDate, dayOfWeek);
			JOptionPane.showMessageDialog(null, "Uspe�no ste postavili popust!", "Uspe�no postavljeno!", JOptionPane.INFORMATION_MESSAGE);
		}
		else if (DiscountOnTest.isSelected()) {
			int viewindex = table.getSelectedRow();
			int index = table.convertRowIndexToModel(viewindex);
			if (index == -1) JOptionPane.showMessageDialog(null, "Morate izabrati koju analizu iz tabele!", "Gre�ka!", JOptionPane.ERROR_MESSAGE);
			else {
				LabTest labTest = ((LabTestTableModel) table.getModel()).getRows().get(index);
				this.managers.getTestdiscountMng().add(group, labTest, discount, fromDate, toDate, dayOfWeek);
				JOptionPane.showMessageDialog(null, "Uspe�no ste postavili popust!", "Uspe�no postavljeno!", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
	}
	private void addGroupBoxValues() {
		ArrayList<String> groups = this.managers.getLabtestMng().getGroups();
		for (String group : groups) {
			groupBox.addItem(group);
		}
		setTableValues();
	}
	
	private void setTableValues() {
		LabTestTableModel tableModel = new LabTestTableModel(this.managers.getLabtestMng(), (String) groupBox.getSelectedItem());
		table.setModel(tableModel);
	}
}
