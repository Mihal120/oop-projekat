package utils;

public class AppSettings {
	private String administratorFilename;
	private String bloodtestFilename;
	private String labtechnicianFilename;
	private String medicalresultFilename;
	private String medicaltechnicianFilename;
	private String patientFilename;
	private String requestFilename;
	private String pricesFilename;
	private String testDiscountFilename;
	
	public AppSettings(String administratorFilename, String bloodtestFilename, String labtechnicianFilename, String medicalresultFilename, String medicaltechnicianFilename,
			String patientFilename, String requestFilename, String pricesFilename, String testDiscountFilename) {
		super();
		this.administratorFilename = administratorFilename;
		this.bloodtestFilename = bloodtestFilename;
		this.labtechnicianFilename = labtechnicianFilename;
		this.medicalresultFilename = medicalresultFilename;
		this.medicaltechnicianFilename = medicaltechnicianFilename;
		this.patientFilename = patientFilename;
		this.requestFilename = requestFilename;
		this.pricesFilename = pricesFilename;
		this.testDiscountFilename = testDiscountFilename;
	}

	
	
	public String getTestDiscountFilename() {
		return testDiscountFilename;
	}


	public String getAdministratorFilename() {
		return administratorFilename;
	}

	public String getBloodtestFilename() {
		return bloodtestFilename;
	}

	public String getLabtechnicianFilename() {
		return labtechnicianFilename;
	}

	public String getMedicalresultFilename() {
		return medicalresultFilename;
	}

	public String getMedicaltechnicianFilename() {
		return medicaltechnicianFilename;
	}

	public String getPatientFilename() {
		return patientFilename;
	}

	public String getRequestFilename() {
		return requestFilename;
	}

	public String getPricesFilename() {
		return pricesFilename;
	}

}
