package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import entity.Format;
import entity.LabTechnician;
import entity.LabTest;
import entity.MedicalResult;
import entity.MedicalTechnician;
import entity.Patient;
import entity.Request;
import entity.State;

public class RequestManager {
	private String requestFile;
	private List<Request> requests;
	private PatientManager patientMng;
	private MedicalTechnicianManager medicaltechMng;
	private MedicalResultManager medicalResMng;
	private TestDiscountManager testDiscountMng;
	private PricesManager priceMng;
	
	public RequestManager(String requestFile, PatientManager patientMng, MedicalTechnicianManager medicaltechMng, MedicalResultManager medicalResMng, TestDiscountManager testDiscountMng, PricesManager priceManager) {
		super();
		this.requestFile = requestFile;
		this.requests = new ArrayList<Request>();
		this.patientMng = patientMng;
		this.medicaltechMng = medicaltechMng;
		this.medicalResMng = medicalResMng;
		this.testDiscountMng = testDiscountMng;
		this.priceMng = priceManager;
	}
	public List<Request> getRequests() {
		return requests;
	}
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.requestFile), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(",");
				int id = Integer.parseInt(tokens[0]);
				Patient patient = patientMng.findPatientById(Integer.parseInt(tokens[1]));
				boolean home_arrival = Boolean.parseBoolean(tokens[2].toLowerCase());
				LocalDateTime taken_date_time = LocalDateTime.parse(tokens[3], Format.date_time_format);
				LocalDateTime finished_date_time = null;
				if (!tokens[4].equals("")) {
					finished_date_time = LocalDateTime.parse(tokens[4], Format.date_time_format);
				}
				MedicalTechnician blood_taker = null;
				if (!tokens[5].equals("")) {
					blood_taker = medicaltechMng.findMedTechById(Integer.parseInt(tokens[5]));
				}
				String[] ids = tokens[6].split(" ");
				ArrayList<MedicalResult> results = this.getResults(ids);
				State cur_state = State.valueOf(tokens[7]);
				Request request = new Request(id, patient, home_arrival, taken_date_time, finished_date_time, blood_taker, results, cur_state);
				this.requests.add(request);
			}
			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveData() {
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(this.requestFile, false));
			for (Request request : this.requests) {
				pw.println(request.toFileString());
			}
			pw.close();			
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void add(Patient patient, boolean homeArrival, LocalDateTime takenDateTime,
			 List<LabTest> tests) {
		int id = getNextId();
		LocalDateTime finishedDateTime = null;
		MedicalTechnician bloodTaker = null;
		State state = State.INITIAL;
		List<MedicalResult> results = new ArrayList<MedicalResult>();
		for (LabTest test : tests) {
			MedicalResult result = new MedicalResult(this.medicalResMng.getNextId(), test, 0, null, null);
			this.medicalResMng.add(result);
			results.add(result);
		}
		this.requests.add(new Request(id, patient, homeArrival, takenDateTime, finishedDateTime, bloodTaker, results, state));
		this.saveData();
	}
	
	public void edit(int id, Patient patient, boolean home_arrival, LocalDateTime takenDateTime, LocalDateTime finishedDateTime,
			MedicalTechnician bloodTaker, List<MedicalResult> results, State state) {
		Request request = this.findRequestById(id);
		request.setPatient(patient);
		request.setHomeArrival(home_arrival);
		request.setTakenDateTime(takenDateTime);
		request.setFinishedDateTime(finishedDateTime);
		request.setBloodTaker(bloodTaker);
		request.setResults(results);
		request.setState(state);
		this.saveData();
	}
	
	
	private int getNextId() {
		int id = this.requests.get(this.requests.size()-1).getId() + 1;
		while (this.hasId(id)) {
			id++;
		}
		return id;
	}
	
	
	
	private boolean hasId(int id) {
		for (Request request : requests) {
			if (request.getId() == id) {
				return true;
			}
		}
		return false;
	}
	
	
	public Request findRequestById(int id) {
		for (Request request : requests) {
			if (request.getId() == id) {
				return request;
			}
		}
		return null;
	}
	
	public ArrayList<Request> getRequestsByPatient(Patient patient) {
		ArrayList<Request> requestList = new ArrayList<Request>();
		if (patient == null) {
			return requestList;
		}
		for (Request request : this.requests) {
			if (request.getPatient().getUsername().equals(patient.getUsername())){
				requestList.add(request);
			}
		}
		return requestList;
	}
	
	private ArrayList<MedicalResult> getResults(String[] ids) {
		ArrayList<MedicalResult> results = new ArrayList<MedicalResult>();
		for (String id : ids) {
			results.add(this.medicalResMng.findMedResultById(Integer.parseInt(id)));
		}
		return results;
	}
	
	public ArrayList<Request> getNotTakenRequests(){
		ArrayList<Request> notTakenRequests = new ArrayList<Request>();
		for (Request request : this.requests) {
			if (request.getBloodTaker() == null || request.getState() == State.INITIAL) {
				notTakenRequests.add(request);
			}
		}
		return notTakenRequests;
	}
	
	public int getMedTechMeasures(MedicalTechnician medTech, Date fromDate, Date toDate, int dayOfWeek, String group, int resultNumber, boolean homeArrival) {
	    int count = 0;
		for (Request request : this.requests) {
			if (request.getFinishedDateTime() != null) {
				Date requestdate = Date.from(request.getTakenDateTime().atZone(ZoneId.systemDefault()).toInstant());
				Calendar c = Calendar.getInstance();
				c.setTime(requestdate);
				int requestDay = c.get(Calendar.DAY_OF_WEEK);
				if (request.getResultGroups().contains(group) && request.getResults().size() > resultNumber && request.isHomeArrival() == homeArrival && 
						medTech.getId() == request.getBloodTaker().getId() && requestdate.after(fromDate) && requestdate.before(toDate) && requestDay == dayOfWeek) {
						count++;
				}
			}
		}
		return count;
	}
	
	
	public ArrayList<Request> getRequestsByParams(Date startDate, Date endDate, int dayOfWeek, String group, int resultNumber, boolean homeArrival){
		ArrayList<Request> date_requests = new ArrayList<Request>();
		for (Request request : this.requests) {
			Date requestdate = Date.from(request.getFinishedDateTime().atZone(ZoneId.systemDefault()).toInstant());
			Calendar c = Calendar.getInstance();
			c.setTime(requestdate);
			int requestDay = c.get(Calendar.DAY_OF_WEEK);
			if (request.getResultGroups().contains(group) && request.getResults().size() > resultNumber && request.isHomeArrival() == homeArrival &&
				requestdate.after(startDate) && requestdate.before(endDate) && requestDay == dayOfWeek) {
					date_requests.add(request);
				}
		
		}
		return date_requests;
	}
	
	public int getLabTechMeasures(LabTechnician labTech, Date fromDate, Date toDate, int dayOfWeek, String group, int resultNumber, boolean homeArrival) {
		ZoneId defaultZoneId = ZoneId.systemDefault();
	    int count = 0;
		for (MedicalResult medicalResult : this.medicalResMng.getMedicalresults()) {
			if (medicalResult.getDate() != null) {
				Date resultdate = Date.from(medicalResult.getDate().atStartOfDay(defaultZoneId).toInstant());
				Calendar c = Calendar.getInstance();
				c.setTime(resultdate);
				int resultDay = c.get(Calendar.DAY_OF_WEEK);
				Request requestOfResult = this.getRequestOfResults(medicalResult);
				if (requestOfResult.getResultGroups().contains(group) && requestOfResult.getResults().size() > resultNumber && requestOfResult.isHomeArrival() == homeArrival && 
						labTech.getId() == medicalResult.getLaboratorian().getId() && resultdate.after(fromDate) && resultdate.before(toDate) && resultDay == dayOfWeek) {
						count++;
				}
			}
		}
		return count;
	}
	
	public ArrayList<MedicalResult> getUnmeasuredForLabTech(LabTechnician labTech){
		ArrayList<MedicalResult> unmeasuredResults = new ArrayList<MedicalResult>();
		for (Request request : this.requests) {
			if (request.getState() == State.PROCESSING) {
				for (MedicalResult result : request.getResults()) {
					if (result.getDate() == null && Arrays.asList(labTech.getSpecializations()).contains(result.getTestType().getGroup())) {
						unmeasuredResults.add(result);
					}
				}				
			}
		}
		return unmeasuredResults;
	}
	
	public void checkForFinishedRequest(MedicalResult result) {
		Request request = this.getRequestOfResults(result);
		boolean finished = true;
		for (MedicalResult medResult : request.getResults()) {
			if (medResult.getDate() == null) finished = false;
		}
		if (finished) {
			request.setFinishedDateTime(LocalDateTime.now());
			this.edit(request.getId(), request.getPatient(), request.isHomeArrival(), request.getTakenDateTime(), request.getFinishedDateTime(), request.getBloodTaker(), request.getResults(), request.getState());
		}
	}
	
	public ArrayList<Patient> getPatientsByDate(Date startDate, Date endDate, String group){
		ArrayList<Patient> patients = new ArrayList<Patient>();
		for (Request request : requests) {
			Date requestdate = Date.from(request.getFinishedDateTime().atZone(ZoneId.systemDefault()).toInstant());
			if (request.getResultGroups().contains(group) && requestdate.after(startDate) && requestdate.before(endDate)) {
				if (!patients.contains(request.getPatient())) {
					patients.add(request.getPatient());
				}
			}
		}
		return patients;
	}
	
	public int getPatientRequestNumber(Date startDate, Date endDate, String group, Patient patient){
		int count = 0;
		for (Request request : requests) {
			Date requestdate = Date.from(request.getFinishedDateTime().atZone(ZoneId.systemDefault()).toInstant());
			if (request.getResultGroups().contains(group) && requestdate.after(startDate) && requestdate.before(endDate)) {
				if (request.getPatient().getId() == patient.getId()) {
					count++;
				}
			}
		}
		return count;
	}
	
	public ArrayList<MedicalResult> getOutOfRangePatientResults(Patient patient){
		ArrayList<MedicalResult> outOfRangeResults = new ArrayList<MedicalResult>();
		ArrayList<Request> patientRequests = this.getRequestsByPatient(patient);
		if (patientRequests.size() < 2) {
			for (int i = 0; i < patientRequests.size(); i++) {
				ArrayList<MedicalResult> results = (ArrayList<MedicalResult>) patientRequests.get(patientRequests.size()-i-1).getResults();
				for (MedicalResult medicalResult : results) {
					if (medicalResult.getMeasured_value() > medicalResult.getTestType().getMaximum_value()) {
						outOfRangeResults.add(medicalResult);
					}
				}
			}
		}
		else {
			for (int i = 0; i < 2; i++) {
				ArrayList<MedicalResult> results = (ArrayList<MedicalResult>) patientRequests.get(patientRequests.size()-i-1).getResults();
				for (MedicalResult medicalResult : results) {
					if (medicalResult.getMeasured_value() > medicalResult.getTestType().getMaximum_value()) {
						outOfRangeResults.add(medicalResult);
					}
				}
			}
		}
		return outOfRangeResults;
	}
	
	public int getPatientTotalPrice(Date startDate, Date endDate, String group, Patient patient){
		int price = 0;
		for (Request request : requests) {
			Date requestdate = Date.from(request.getFinishedDateTime().atZone(ZoneId.systemDefault()).toInstant());
			if (request.getResultGroups().contains(group) && requestdate.after(startDate) && requestdate.before(endDate)) {
				if (request.getPatient().getId() == patient.getId()) {
					price += request.getSummedPrice(this.testDiscountMng, this.priceMng);
				}
			}
		}
		return price;
	}
	
	public void getPreviuosMeasures(ArrayList<Date> xData, ArrayList<Double> yData, Request selectedRequest, MedicalResult selectedResult) {
		for (Request request : this.requests) {
			if (request.getPatient().getId() == selectedRequest.getPatient().getId()) {
				this.findResultData(xData, yData, selectedResult.getTestType(), request);
			}
		}
	}
	private void findResultData(ArrayList<Date> xData, ArrayList<Double> yData, LabTest test, Request request) {
		for (MedicalResult result : request.getResults()) {
			if (result.getTestType().getId() == test.getId()) {
				Date date = Date.from(result.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
				xData.add(date);
				yData.add((double) result.getMeasured_value());
			}
		}
	}
	
	public void getStatistics(ArrayList<Date> xData, ArrayList<Double> yData, Date fromDate, Date toDate, int fromYear, int toYear, LabTest test, String gender) {
		ArrayList<Date> dates = new ArrayList<Date>();
		Calendar cal = Calendar.getInstance();
		Date date = fromDate;
		cal.setTime(date);
		dates.add(fromDate);
		while (true) {
			cal.add(Calendar.DAY_OF_YEAR, 14);
			date = cal.getTime();
			if (date.after(toDate))break;
			dates.add(date);
		}
		dates.add(toDate);
		if (dates.size() == 2) {
			ArrayList<Patient> patients = new ArrayList<Patient>();
			countPatients(fromYear, toYear, test, gender, patients, fromDate, toDate);
			xData.add(toDate);
			yData.add((double) patients.size());
		}
		else {
			for (int i = 0; i < dates.size()-1; i++) {
				ArrayList<Patient> patients = new ArrayList<Patient>();
				Date startDate = dates.get(i);
				Date endDate = dates.get(i+1);
				countPatients(fromYear, toYear, test, gender, patients, startDate, endDate);
				xData.add(endDate);
				yData.add((double) patients.size());
			}
		}
		sortData(xData, yData);
	}
	
	private void countPatients(int fromYear, int toYear, LabTest test, String gender, ArrayList<Patient> patients,
			Date startDate, Date endDate) {
		for (Request request : this.requests) {
			Date requestDate = Date.from(request.getTakenDateTime().atZone(ZoneId.systemDefault()).toInstant());
			if (requestDate.after(startDate) && requestDate.before(endDate) && request.containsTest(test)) {
				Patient patient = request.getPatient();
				LocalDate now = LocalDate.now();
				long years = java.time.temporal.ChronoUnit.YEARS.between( patient.getBirth_date() , now );
				if (gender.equalsIgnoreCase("MALE") || gender.equalsIgnoreCase("FEMALE")) {
					if (patient.getGender().name().equalsIgnoreCase(gender) &&  fromYear <= years && years < toYear && !patients.contains(patient))patients.add(patient);
				}
				else if (fromYear <= years && years < toYear && !patients.contains(patient))patients.add(patient);					
			}
		}
	}
	
	private void sortData(ArrayList<Date> xData, ArrayList<Double> yData) {
		for (int i = 0; i < xData.size()-1; i++) {
			for (int j = i+1; j < xData.size(); j++) {
				if (xData.get(i).compareTo(xData.get(j)) > 0) {
					Date tempx = xData.get(i);
					xData.set(i, xData.get(j));
					xData.set(j, tempx);
					double tempy = yData.get(i);
					yData.set(i, yData.get(j));
					yData.set(j, tempy);
				}
			}
		}
	}
	
	private Request getRequestOfResults(MedicalResult medicalResult) {
		for (Request request : this.requests) {
			for (MedicalResult result : request.getResults()) {
				if (result.getId() == medicalResult.getId()) {
					return request;
				}
			}
		}
		return null;
	}
	
	
}
