package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import entity.LabTest;

public class LabTestManager {
	private String labtestFile;
	private List<LabTest> labTests;
	
	public LabTestManager(String bloodtestFile) {
		super();
		this.labtestFile = bloodtestFile;
		this.labTests = new ArrayList<LabTest>();
	}

	public List<LabTest> getLabTests() {
		return this.labTests;
	}
	
	public LabTest findLabTestById(int id) {
		for (LabTest test : this.labTests) {
			if (id == test.getId()) {
				return test;
			}
		}
		return null;
	}
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.labtestFile), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(",");
				String group = String.valueOf(tokens[1]);
				LabTest bt = new LabTest(Integer.parseInt(tokens[0]), group, tokens[2], Integer.parseInt(tokens[3]), tokens[4], Float.parseFloat(tokens[5]), Float.parseFloat(tokens[6]));
				this.labTests.add(bt);
			}
			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveData() {
		try {
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(this.labtestFile), "UTF-8"), false);
			for (LabTest test : this.labTests) {
				pw.println(test.toFileString());
			}
			pw.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void add(int id, String group, String type, int price, String unit, float minimum_value,
			float maximum_value) {
		this.labTests.add(new LabTest(id, group, type, price, unit, minimum_value, maximum_value));
		this.saveData();
	}
	
	public void edit(int id, String group, String type, int price, String unit, float minimum_value,
			float maximum_value) {
		LabTest test = this.findLabTestById(id);
		test.setGroup(group);
		test.setName(type);
		test.setUnit(unit);
		test.setPrice(price);
		test.setMaximum_value(maximum_value);
		test.setMinimum_value(minimum_value);
		this.saveData();
	}
	
	public ArrayList<String> getGroups() {
		ArrayList<String> groups = new ArrayList<String>();
		for (LabTest test : this.labTests) {
			if (!groups.contains(test.getGroup())) {
				groups.add(test.getGroup());				
			}
		}
		return groups;
	}
	
	public ArrayList<LabTest> getLabTestsByGroup(String group){
		ArrayList<LabTest> tests = new ArrayList<LabTest>();
		for (LabTest labTest : this.labTests) {
			if (labTest.getGroup().equals(group)) {
				tests.add(labTest);
			}
		}
		return tests;
	}
	
	public ArrayList<String> getAnalysis(){
		ArrayList<String> analysis = new ArrayList<String>();
		for (LabTest test : this.labTests) {
			if (!analysis.contains(test.getName())) {
				analysis.add(test.getName());				
			}
		}
		return analysis;
	}
	
	public LabTest getLabTestByName(String name) {
		for (LabTest labTest : this.labTests) {
			if (labTest.getName().equals(name)) {
				return labTest;
			}
		}
		return null;
	}
	
}
