package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import entity.Format;
import entity.Gender;
import entity.MedicalTechnician;

public class MedicalTechnicianManager {
	private String medicaltechnicianFile;
	private List<MedicalTechnician> medTechnicians;

	public MedicalTechnicianManager(String medicaltechnicianFile) {
		this.medicaltechnicianFile = medicaltechnicianFile;
		this.medTechnicians = new ArrayList<MedicalTechnician>();
	}

	public List<MedicalTechnician> getMedTechnicians() {
		return medTechnicians;
	}
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.medicaltechnicianFile), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(",");
				int id = Integer.parseInt(tokens[0]);
				String name = tokens[1];
				String surname = tokens[2];
				String username = tokens[3];
				String password = tokens[4];
				Gender gender = Gender.valueOf(tokens[5]);
				LocalDate birth_date = LocalDate.parse(tokens[6], Format.date_format);
				String phone_number = tokens[7];
				String address = tokens[8];
				int qualification = Integer.parseInt(tokens[9]);
				int internship = Integer.parseInt(tokens[10]);
				int basis_salary = Integer.parseInt(tokens[11]);
				int bonus_salary = Integer.parseInt(tokens[12]);
				MedicalTechnician med_tech = new MedicalTechnician(id, name, surname, username, password, gender, birth_date, phone_number, address, qualification, internship, basis_salary, bonus_salary);
				this.medTechnicians.add(med_tech);
			}
			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveData() {
		try {
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(this.medicaltechnicianFile), "UTF-8"), false);
			for (MedicalTechnician med_tech: this.medTechnicians) {
				pw.println(med_tech.toFileString());
			}
			pw.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void add(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, 
			int qualification, int internship, int basis_salary, int bonus_salary) {
		this.medTechnicians.add(new MedicalTechnician(id, name, surname, username, password, gender, birth_date, phone_number, address, qualification, internship, basis_salary, bonus_salary));
		this.saveData();
	}

	public void edit(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, 
			int qualification, int internship, int basis_salary, int bonus_salary) {
		MedicalTechnician med_tech = this.findMedTechById(id);
		med_tech.setName(name);
		med_tech.setSurname(surname);
		med_tech.setUsername(username);
		med_tech.setPassword(password);
		med_tech.setBirth_date(birth_date);
		med_tech.setGender(gender);
		med_tech.setPhone_number(phone_number);
		med_tech.setAddress(address);
		med_tech.setBasis_salary(basis_salary);
		med_tech.setBonus_salary(bonus_salary);
		med_tech.setInternship(internship);
		med_tech.setQualification(qualification);
		this.saveData();
	}
	
	
	public MedicalTechnician findMedTechById(int id) {
		for (MedicalTechnician med_tech : this.medTechnicians) {
			if (med_tech.getId() == id) {
				return med_tech;
			}
		}
		return null;
	}


}
