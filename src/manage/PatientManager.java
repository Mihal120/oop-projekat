package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import entity.Format;
import entity.Gender;
import entity.Patient;

public class PatientManager {
	private String patientFile;
	private List<Patient> patients;
	
	public PatientManager(String patientFile) {
		super();
		this.patientFile = patientFile;
		this.patients = new ArrayList<Patient>();
	}

	public List<Patient> getPatients() {
		return this.patients;
	}
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.patientFile), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(",");
				int id = Integer.parseInt(tokens[0]);
				String name = tokens[1];
				String surname = tokens[2];
				String username = tokens[3];
				String password = tokens[4];
				Gender gender = Gender.valueOf(tokens[5]);
				LocalDate birth_date = LocalDate.parse(tokens[6], Format.date_format);
				String phone_number = tokens[7];
				String address = tokens[8];
				String LBO = tokens[9];
				Patient patient = new Patient(id, name, surname, username, password, gender, birth_date, phone_number, address, LBO);
				this.patients.add(patient);
			}
			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveData() {
		try {
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(this.patientFile), "UTF-8"), false);
			for (Patient patient : this.patients) {
				pw.println(patient.toFileString());
			}
			pw.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void add(String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, String LBO) {
		int id = this.getNextId();
		this.patients.add(new Patient(id, name, surname, username, password, gender, birth_date, phone_number, address, LBO));
		this.saveData();
	}
	
	public void edit(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, String LBO) {
		Patient patient = this.findPatientById(id);
		patient.setName(name);
		patient.setSurname(surname);
		patient.setUsername(username);
		patient.setPassword(password);
		patient.setGender(gender);
		patient.setBirth_date(birth_date);
		patient.setPhone_number(phone_number);
		patient.setAddress(address);
		patient.setLBO(LBO);
		this.saveData();
		
	}
	
	
	private int getNextId() {
		int id = this.patients.get(this.patients.size()-1).getId() + 1;
		while (this.hasId(id)) {
			id++;
		}
		return id;
	}
	
	
	
	private boolean hasId(int id) {
		for (Patient patient : this.patients) {
			if (patient.getId() == id) {
				return true;
			}
		}
		return false;
	}
	
	public Patient findPatientById(int id) {
		for (Patient patient : this.patients) {
			if (patient.getId() == id) {
				return patient;
			}
		}
		return null;
	}
	
	public Patient findPatientByLbo(String lbo) {
		for (Patient patient : this.patients) {
			if (patient.getLBO().equals(lbo)) {
				return patient;
			}
		}
		return null;
	}
}
