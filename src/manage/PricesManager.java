package manage;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import entity.Prices;

public class PricesManager {
	private String pricesFile;
	private Prices prices;
	public PricesManager(String pricesFile) {
		super();
		this.pricesFile = pricesFile;
	}
	public Prices getPrices() {
		return this.prices;
	}
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(this.pricesFile));
			String line = br.readLine();
			String[] tokens = line.split(",");
			float homeArrivalPrice = Float.parseFloat(tokens[0]);
			float timePrice = Float.parseFloat(tokens[1]);
			float[] qualificationCoefficients = new float[8];
			for (int i = 0; i < qualificationCoefficients.length; i++) {
				qualificationCoefficients[i] = Float.parseFloat(tokens[i+2]);
			}
			this.prices = new Prices(homeArrivalPrice, timePrice, qualificationCoefficients);
			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	public boolean saveData() {
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(this.pricesFile, false));
			pw.println(this.prices.toFileString());
			pw.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void edit(float homeArrivalPrice, float timePrice, float[] qualificationCoefficients) {
		this.prices.setHomeArrivalPrice(homeArrivalPrice);
		this.prices.setTimePrice(timePrice);
		this.prices.setQualificationCoefficients(qualificationCoefficients);
		this.saveData();
	}
	
	public void editQualificationCoefficient(int qualification, float coefficient) {
		float[] qualificationCoefficient = this.prices.getQualificationCoefficients();
		qualificationCoefficient[qualification] = coefficient;
		this.prices.setQualificationCoefficients(qualificationCoefficient);
		this.saveData();
	}

}
