package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import entity.Format;
import entity.LabTest;
import entity.TestDiscount;

public class TestDiscountManager {
	private String testDiscountFile;
	private LabTestManager testMng;
	private ArrayList<TestDiscount> testDiscounts;
	
	
	public TestDiscountManager(String testDiscountFile,  LabTestManager testMng) {
		super();
		this.testDiscountFile = testDiscountFile;
		this.testMng = testMng;
		this.testDiscounts = new ArrayList<TestDiscount>();
	}
	
	public ArrayList<TestDiscount> getTestDiscounts() {
		return this.testDiscounts;
	}
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.testDiscountFile), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(",");
				int id = Integer.parseInt(tokens[0]);
				String group = tokens[1];
				LabTest test = null;
				if (!tokens[2].equals("")) {
					test = this.testMng.findLabTestById(Integer.parseInt(tokens[2]));					
				}
				float discount = Float.parseFloat(tokens[3]);
				Date startDate = Format.sdf.parse(tokens[4]);
				Date endDate = Format.sdf.parse(tokens[5]);
				int dayOfWeek = Integer.parseInt(tokens[6]);
				TestDiscount td = new TestDiscount(id, group, test, discount, startDate, endDate, dayOfWeek);
				this.testDiscounts.add(td);
			}
			br.close();
		} catch (IOException | ParseException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveData() {
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(this.testDiscountFile), "UTF-8"), false);
			for (TestDiscount discount : this.testDiscounts) {
				pw.println(discount.toFileString());
			}
			pw.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void add(String group, LabTest test, float discount, Date startDate, Date endDate, int dayOfWeek) {
		this.testDiscounts.add(new TestDiscount(this.getNextId(), group, test, discount, startDate, endDate, dayOfWeek));
		this.saveData();
	}
	
	public void edit(int id, String group, LabTest test, float discount, Date startDate, Date endDate, int dayOfWeek) {
		TestDiscount testDiscount = this.findById(id);
		testDiscount.setGroup(group);
		testDiscount.setTest(test);
		testDiscount.setDiscount(discount);
		testDiscount.setStartDate(startDate);
		testDiscount.setEndDate(endDate);
		testDiscount.setDayOfWeek(dayOfWeek);
		this.saveData();
	}
	
	public TestDiscount findById(int id) {
		for (TestDiscount testDiscount : this.testDiscounts) {
			if (id == testDiscount.getId()) {
				return testDiscount;
			}
		}
		return null;
	}
	
	public float findTestDiscount(LabTest labTest, Date requestDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(requestDate);
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		int day = dayOfWeek - 2;
		if (dayOfWeek == 1) {
			day = 6;
		}
		float discount = 0;
		for (TestDiscount testDiscount : this.testDiscounts) {
			if (requestDate.after(testDiscount.getStartDate()) && requestDate.before(testDiscount.getEndDate()) && day == testDiscount.getDayOfWeek()) {
				if (testDiscount.getGroup().equals(labTest.getGroup()) && testDiscount.getTest() == null) {
					discount += testDiscount.getDiscount();
				}
				else if (testDiscount.getGroup().equals(labTest.getGroup()) && testDiscount.getTest().getId() == labTest.getId()) {
					discount += testDiscount.getDiscount();
				}
			}
		}
		return labTest.getPrice()*discount;
	}
	
	
	private int getNextId() {
		if (this.testDiscounts.size() == 0) {
			return 1;
		}
		int id = this.testDiscounts.get(this.testDiscounts.size()-1).getId() + 1;
		while (this.hasId(id)) {
			id++;
		}
		return id;
	}
	
	private boolean hasId(int id) {
		for (TestDiscount discount : this.testDiscounts) {
			if (discount.getId() == id) {
				return true;
			}
		}
		return false;
	}
	
}
