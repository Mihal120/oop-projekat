package manage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import entity.Administrator;
import entity.Gender;

public class AdministratorManager {
	private String administratorFile;
	private List<Administrator> administrators;
	
	public AdministratorManager(String administratorFile) {
		super();
		this.administratorFile = administratorFile;
		this.administrators = new ArrayList<Administrator>();
	}

	public List<Administrator> getAdministrators() {
		return this.administrators;
	}
	
	
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.administratorFile), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(",");
				Gender gender = Gender.valueOf(tokens[5]);
				LocalDate birth_date = LocalDate.parse(tokens[6], DateTimeFormatter.ofPattern("dd.MM.yyyy."));
				Administrator admin = new Administrator(Integer.parseInt(tokens[0]), tokens[1], tokens[2], tokens[3], tokens[4], gender, birth_date, tokens[7], tokens[8]);
				this.administrators.add(admin);
			}
			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveData() {
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(this.administratorFile), "UTF-8"), false);
			for (Administrator admin : this.administrators) {
				pw.println(admin.toFileString());
			}
			pw.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void add(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address) {
		this.administrators.add(new Administrator(id, name, surname, username, password, gender, birth_date, phone_number, address));
		this.saveData();
	}
	
	public void edit(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address) {
		Administrator admin = this.findAdminById(id);
		admin.setName(name);
		admin.setSurname(surname);
		admin.setUsername(username);
		admin.setPassword(password);
		admin.setGender(gender);
		admin.setBirth_date(birth_date);
		admin.setPhone_number(phone_number);
		admin.setAddress(address);
		this.saveData();
	}
	
	public Administrator findAdminById(int id) {
		for (Administrator admin : this.administrators) {
			if (id == admin.getId()) {
				return admin;
			}
		}
		return null;
	}
	
}
