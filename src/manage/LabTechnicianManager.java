package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import entity.Format;
import entity.Gender;
import entity.LabTechnician;

public class LabTechnicianManager {
	private String labtechnicianFile;
	private List<LabTechnician> labTechnicians;
	
	public LabTechnicianManager(String labtechnicianFile) {
		super();
		this.labtechnicianFile = labtechnicianFile;
		this.labTechnicians = new ArrayList<LabTechnician>();		
	}

	public List<LabTechnician> getLabTechnicians() {
		return this.labTechnicians;
	}
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.labtechnicianFile), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(",");
				int id = Integer.parseInt(tokens[0]);
				String name = tokens[1];
				String surname = tokens[2];
				String username = tokens[3];
				String password = tokens[4];
				Gender gender = Gender.valueOf(tokens[5]);
				LocalDate birth_date = LocalDate.parse(tokens[6], Format.date_format);
				String phone_number = tokens[7];
				String address = tokens[8];
				int qualification = Integer.parseInt(tokens[9]);
				int internship = Integer.parseInt(tokens[10]);
				int basis_salary = Integer.parseInt(tokens[11]);
				int bonus_salary = Integer.parseInt(tokens[12]);
				String[] specializations = tokens[13].split(" ");
				LabTechnician lt = new LabTechnician(id, name, surname, username, password, gender,
						birth_date, phone_number, address, qualification, internship, basis_salary, bonus_salary, specializations);
				this.labTechnicians.add(lt);
			}
			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveData() {
		try {
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(this.labtechnicianFile), "UTF-8"), false);
			for (LabTechnician lab_tech : this.labTechnicians) {
				pw.println(lab_tech.toFileString());
			}
			pw.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void add(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, 
			int qualification, int internship, int basis_salary, int bonus_salary, String[] specializations) {
		this.labTechnicians.add(new LabTechnician(id, name, surname, username, password, gender, birth_date, phone_number, address, 
			qualification, internship, basis_salary, bonus_salary, specializations));
		this.saveData();
	}
	
	public void edit(int id, String name, String surname, String username, String password, Gender gender,
			LocalDate birth_date, String phone_number, String address, 
			int qualification, int internship, int basis_salary, int bonus_salary, String[] specializations) {
		LabTechnician lab_tech = this.findLabTechById(id);
		lab_tech.setName(name);
		lab_tech.setSurname(surname);
		lab_tech.setUsername(username);
		lab_tech.setPassword(password);
		lab_tech.setBirth_date(birth_date);
		lab_tech.setGender(gender);
		lab_tech.setPhone_number(phone_number);
		lab_tech.setAddress(address);
		lab_tech.setBasis_salary(basis_salary);
		lab_tech.setBonus_salary(bonus_salary);
		lab_tech.setInternship(internship);
		lab_tech.setQualification(qualification);
		lab_tech.setSpecializations(specializations);
		this.saveData();
	}
	
	
	public LabTechnician findLabTechById(int id) {
		for (LabTechnician lab_tech : this.labTechnicians) {
			if (lab_tech.getId() == id) {
				return lab_tech;
			}
		}
		return null;
	}
	
	
}
