package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import entity.LabTest;
import entity.Format;
import entity.LabTechnician;
import entity.MedicalResult;

public class MedicalResultManager {
	private String medicalresultFile;
	private List<MedicalResult> medicalresults;
	private LabTestManager labTestMng;
	private LabTechnicianManager labTechMng;
	
	public MedicalResultManager(String medicalresultFile, LabTestManager labtestMng, LabTechnicianManager labTechMng) {
		this.medicalresultFile = medicalresultFile;
		this.medicalresults = new ArrayList<MedicalResult>();
		this.labTestMng = labtestMng;
		this.labTechMng = labTechMng;
	}

	public List<MedicalResult> getMedicalresults() {
		return this.medicalresults;
	}
	
	public boolean loadData() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.medicalresultFile), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(",");
				int id = Integer.parseInt(tokens[0]);
				LabTest test_type = labTestMng.findLabTestById(Integer.parseInt(tokens[1]));
				float measured_value = Float.parseFloat(tokens[2]);
				LocalDate date = null;
				LabTechnician lt = null;
				if (tokens.length == 5) {
					date = LocalDate.parse(tokens[3], Format.date_format);
					lt = this.labTechMng.findLabTechById(Integer.parseInt(tokens[4]));					
				}
				MedicalResult med_res = new MedicalResult(id, test_type, measured_value, date, lt);
				this.medicalresults.add(med_res);
			}
			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveData() {
		try {
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(this.medicalresultFile), "UTF-8"), false);
			for (MedicalResult med_res : this.medicalresults) {
				pw.println(med_res.toFileString());
			}
			pw.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void add(int id, LabTest test_type, float measured_value, LocalDate date, LabTechnician lt) {
		this.medicalresults.add(new MedicalResult(id, test_type, measured_value, date, lt));
		this.saveData();
	}
	
	public void add(MedicalResult result) {
		this.medicalresults.add(result);
		this.saveData();
	}
	
	public void edit(int id, LabTest test_type, float measured_value, LocalDate date, LabTechnician lt) {
		MedicalResult med_result = findMedResultById(id);
		med_result.setMeasured_value(measured_value);
		med_result.setTestType(test_type);
		med_result.setDate(date);
		med_result.setLaboratorian(lt);
		this.saveData();
	}
	
	public MedicalResult findMedResultById(int id) {
		for (MedicalResult med_result : this.medicalresults) {
			if (med_result.getId() == id) {
				return med_result;
			}
		}
		return null;
	}
	

	public int getNextId() {
		int id = this.medicalresults.get(this.medicalresults.size()-1).getId() + 1;
		while (this.hasId(id)) {
			id++;
		}
		return id;
	}
	
	private boolean hasId(int id) {
		for (MedicalResult result : this.medicalresults) {
			if (result.getId() == id) {
				return true;
			}
		}
		return false;
	}
}
