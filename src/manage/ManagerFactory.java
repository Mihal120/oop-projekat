package manage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import entity.Employee;
import entity.LabTechnician;
import entity.MedicalTechnician;
import entity.User;
import utils.AppSettings;

public class ManagerFactory {
	private AppSettings appSettings;
	private AdministratorManager adminMng;
	private LabTestManager labtestMng;
	private LabTechnicianManager labtechMng;
	private MedicalTechnicianManager medtechMng;
	private MedicalResultManager medresMng;
	private PatientManager patientMng;
	private RequestManager requestMng;
	private PricesManager pricesMng;
	private TestDiscountManager testdiscountMng;
	
	public ManagerFactory(AppSettings appSettings) {
		this.appSettings = appSettings;
		this.adminMng = new AdministratorManager(this.appSettings.getAdministratorFilename());
		this.labtestMng = new LabTestManager(this.appSettings.getBloodtestFilename());
		this.labtechMng = new LabTechnicianManager(this.appSettings.getLabtechnicianFilename());
		this.medtechMng = new MedicalTechnicianManager(this.appSettings.getMedicaltechnicianFilename());
		this.medresMng = new MedicalResultManager(this.appSettings.getMedicalresultFilename(), this.labtestMng, this.labtechMng);
		this.patientMng = new PatientManager(this.appSettings.getPatientFilename());
		this.testdiscountMng = new TestDiscountManager(this.appSettings.getTestDiscountFilename(), this.labtestMng);
		this.pricesMng = new PricesManager(this.appSettings.getPricesFilename());
		this.requestMng = new RequestManager(this.appSettings.getRequestFilename(), this.patientMng, this.medtechMng, this.medresMng, this.testdiscountMng, this.pricesMng);
	}
	
	
	public TestDiscountManager getTestdiscountMng() {
		return testdiscountMng;
	}


	public PricesManager getPricesMng() {
		return this.pricesMng;
	}


	public AdministratorManager getAdminMng() {
		return this.adminMng;
	}


	public LabTestManager getLabtestMng() {
		return this.labtestMng;
	}


	public LabTechnicianManager getLabtechMng() {
		return this.labtechMng;
	}


	public MedicalTechnicianManager getMedtechMng() {
		return this.medtechMng;
	}


	public MedicalResultManager getMedresMng() {
		return this.medresMng;
	}


	public PatientManager getPatientMng() {
		return this.patientMng;
	}


	public RequestManager getRequestMng() {
		return this.requestMng;
	}

	
	public void printAll() {
		System.out.println(this.adminMng.getAdministrators());
		System.out.println(this.labtestMng.getLabTests());
		System.out.println(this.labtechMng.getLabTechnicians());
		System.out.println(this.medtechMng.getMedTechnicians());
		System.out.println(this.medresMng.getMedicalresults());
		System.out.println(this.patientMng.getPatients());
		System.out.println(this.requestMng.getRequests());
		System.out.println(this.testdiscountMng.getTestDiscounts());
	}
	
	public User checkLogIn(String username, String password) {
		ArrayList<User> users = new ArrayList<User>();
		users.addAll(this.getAdminMng().getAdministrators());
		users.addAll(this.getLabtechMng().getLabTechnicians());
		users.addAll(this.getMedtechMng().getMedTechnicians());
		users.addAll(this.getPatientMng().getPatients());
		for (User user : users) {
			if (user.getUsername().equals(username) && user.getPassword().equals(password) ) {
				return user;
			}
		}
		return null;
	}
	
	public boolean userExists(String username) {
		ArrayList<User> users = new ArrayList<User>();
		users.addAll(this.getAdminMng().getAdministrators());
		users.addAll(this.getLabtechMng().getLabTechnicians());
		users.addAll(this.getMedtechMng().getMedTechnicians());
		users.addAll(this.getPatientMng().getPatients());
		for (User user : users) {
			if (user.getUsername().equals(username))return true;
		}
		return false;
	}
	


	public void loadData() {
		this.adminMng.loadData();
		this.labtestMng.loadData();
		this.labtechMng.loadData();
		this.medtechMng.loadData();
		this.medresMng.loadData();
		this.patientMng.loadData();
		this.requestMng.loadData();
		this.pricesMng.loadData();
		this.testdiscountMng.loadData();
	}
	
	public float getEmployeesSalary(Employee emp, Date fromDate, Date toDate, int dayOfWeek, String group, int resultsNumber, boolean homeArrival) {
		Employee employee = null;
		int days = getDaysOfWeek(fromDate, toDate, dayOfWeek);
		int count = 0;
		if (emp instanceof LabTechnician) {
			employee = (LabTechnician) emp;
			count = this.getRequestMng().getLabTechMeasures((LabTechnician) employee, fromDate, toDate, dayOfWeek, group, resultsNumber, homeArrival);
		}
		else {
			employee = (MedicalTechnician) emp;
			count = this.getRequestMng().getMedTechMeasures((MedicalTechnician) employee, fromDate, toDate, dayOfWeek, group, resultsNumber, homeArrival);
		}
		float salary = count*employee.getBonus_salary()+employee.getBasis_salary()*employee.getQualification()*employee.getInternship();
		return (salary/30)*days;
	}


	private int getDaysOfWeek(Date fromDate, Date toDate, int dayOfWeek) {
		int days = 0;
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTime(fromDate);
		end.setTime(toDate);
		while (start.before(end)) {
			int day = start.get(Calendar.DAY_OF_WEEK);
			if (day == dayOfWeek) {
				days++;
				start.add(Calendar.DATE, 7);
			}
			else start.add(Calendar.DATE, 1);
		}
		return days;
	}
	
}
